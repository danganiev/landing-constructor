=========
Commands:

celery flower -A landing_constructor --port=5556
celery -A landing_constructor worker -l info
docker cp landing_constructor_container:/tmp/landing /tmp/
./node_modules/.bin/nodemon --nolazy --require @babel/register --inspect=42894 src/index.js


mutation{
  createUser(email:"admin@admin.com", password:"database1"){
    id
  }
}

======
Notes:

before yarn upgrade grapesjs I need to rm -rf node_modules/grapesjs/

When creating section sectionId database row must be the same as in 'data-landing-sectionid' html attribute

==============
Backend notes:

I don't need to install stuff with yarn, i only need to build stuff from docker for backend, or stuff like
bcrypt won't work

========
Backups:

Context - top folder

#This don't go to git
Full db backup: make backup
Restore full backup: make restore_backup

#This does
Default data creation: make default_data
Restore default data: make restore_default_data

# Data only (so schema needs to be up and table manually cleaned)
Default sections: make default_sections
Clean sections: make clean_sections
Restore default sections: make restore_sections


