import React from 'react';
import { map } from 'lodash';
import styled from 'styled-components';
import { connect } from 'react-redux';

import TraitControl from 'traits/TraitControl';
import Button from 'components/Button';
import { toggleComponentSettings } from 'reducers/editor';
import IntegerInput from 'traits/inputs/Integer';
import constants from 'utils/constants';

const SidebarDiv = styled.div`
  position: fixed;
  right: 0;
  top: 60px;
  height: calc(100% - 60px);
  width: 250px;
  background: ${constants.GREY};
  z-index: 100;
  border-left: 1px ${constants.DARK_GREY} solid;
  box-shadow: -2px 0px 2px -1px rgba(0, 0, 0, 0.1);
  display: ${props => (props.toggled ? 'default' : 'none')};
`;

const Header = styled.h2`
  padding: 10px;
  font-weight: 300;
  font-size: 20px;
  height: 20px;
`;

const CloseLayer = styled.div`
  position: fixed;
  z-index: 90;
  width: 100%;
  padding: 0;
  top: 0;
  background-color: #000;
  opacity: 0.01;
  height: 100%;
`;

const TraitsSidebar = ({
  editor,
  selectedComponent,
  componentSettingsToggled,
  dispatch,
  sectionsAndTraits,
  site,
  savePage,
  token
}) => (
  <>
    <SidebarDiv toggled={componentSettingsToggled}>
      <Header>Настройки секции</Header>
      {/* <IntegerInput
        name="top"
        label="Top"
        onChange={event => {
          console.log(event.target.value);
          selectedComponent.setStyle({ position: 'absolute', top: `${event.target.value}px` });
        }}
      />
      <IntegerInput
        name="id trait"
        label="Id trait"
        onChange={event => {
          console.log(selectedComponent.get('attributes'));
          selectedComponent.setAttributes({ abcd: 'pog' });
          // verticalAlign(selectedComponent, 'middle');
        }}
      /> */}
      {selectedComponent && selectedComponent.get('type') === 'section' ? (
        <div>
          {map(
            sectionsAndTraits[selectedComponent.get('attributes')['data-landing-sectionid']],
            (value, key) => (
              <TraitControl
                key={key}
                controlName={value.traitName || key}
                selectedComponent={selectedComponent}
                traitsData={value}
                editor={editor}
                site={site}
                token={token}
              />
            )
          )}
        </div>
      ) : null}
      <span style={{ marginLeft: '7px' }}>
        <Button
          text="Сохранить и закрыть"
          onClick={() => {
            savePage(true);

            dispatch(toggleComponentSettings(false));
          }}
        />
      </span>
    </SidebarDiv>
    <CloseLayer
      style={{ display: componentSettingsToggled ? 'inherit' : 'none' }}
      onClick={() => dispatch(toggleComponentSettings(false))}
    />
  </>
);

const mapStateToProps = state => ({
  editor: state.editor.editor,
  selectedComponent: state.editor.selectedComponent,
  componentSettingsToggled: state.editor.componentSettingsToggled,
  sectionsAndTraits: state.editor.sectionsAndTraits,
  token: state.auth.token
});

export default connect(mapStateToProps)(TraitsSidebar);
