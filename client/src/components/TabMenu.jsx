import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import _ from 'lodash';

import constants from 'utils/constants';

const MenuDiv = styled.div`
  width: 150px;
  background-color: white;
  margin-left: 1px;
`;

const MenuUl = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: flex-start;
  border-right: 1px solid black;
`;

const MenuLi = styled.li``;

const MenuLink = styled.span`
  display: block;
  padding: 1em;
  background-color: white;
  text-align: center;
  text-decoration: none;
  color: #000000;
  cursor: pointer;

  :hover {
    background-color: ${constants.BLUE};
    color: white;
    margin-left: -1px;
  }
`;

export const Tab = styled.div`
  padding: 10px;
`;

const View = styled.div`
  width: calc(100% - 152px);
  background-color: white;
`;

export const Menu = ({ links, setSelectedTab }) => (
  <MenuDiv>
    <MenuUl>
      {_.map(links, (value, id) => (
        <MenuLi key={id}>
          <MenuLink onClick={() => setSelectedTab(id)}>{value}</MenuLink>
        </MenuLi>
      ))}
    </MenuUl>
  </MenuDiv>
);

const MenuView_ = ({ component, componentsMap, ...props }) => {
  const Tag = componentsMap[component];
  return (
    <View>
      <Tag {...props} />
    </View>
  );
};

const mapStateToProps = state => ({
  token: state.auth.token
});

export const MenuView = connect(mapStateToProps)(MenuView_);
