import React from 'react';
import styled from 'styled-components';
import { Field } from 'formik';
import { NavLink } from 'react-router-dom';

export const ParentContainer = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #3f588a;
`;

export const FormDiv = styled.div`
  display: flex;
  justify-content: center;
  width: 400px;
`;

export const DivBlockInside = styled.div`
  padding: 10px;
`;

export const LogoDiv = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  height: 50px;
`;

export const LinkDiv = styled.div`
  width: 310px;
  margin-top: 20px;
  margin-bottom: 30px;
  display: flex;
  justify-content: space-between;
  font-size: 14px;
`;

export const InputField = styled(({ isError, ...rest }) => <Field {...rest} />)`
  height: 30px;
  padding: 5px;
  width: 300px;
  border: 2px solid #ccc;
  border-color: ${props => (props.isError ? 'red' : '#ccc')};
  -webkit-border-radius: 5px;
  border-radius: 5px;
  font-size: 14px;
  margin-bottom: 10px;

  &:focus {
    border-color: ${props => (props.isError ? 'red' : '#333')};
    outline: 0;
  }
`;

export const Label = styled.label``;

export const AuthLink = styled(props => <NavLink {...props} />)`
  color: rgb(0, 0, 238);
`;

export const AuthInput = ({ label, ...props }) => {
  return (
    <>
      <div style={{ paddingBottom: '3px' }}>
        <Label>{label}</Label>
      </div>
      <div>
        <InputField {...props} />
      </div>
    </>
  );
};
