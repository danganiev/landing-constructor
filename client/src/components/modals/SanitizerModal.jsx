import React, { useState } from 'react';
import styled from 'styled-components';
import { each, keys, sortBy } from 'lodash';

import Button from 'components/Button';
import Checkbox from 'components/inputs/Checkbox';
import Modal from 'components/modals/Modal';

const Header = styled.h3`
  padding-bottom: 10px;
  margin-top: 0px;
  width: 100%;
  text-align: center;
`;

const SanitizerModal = ({ isSanitizerOpen, toggleSanitizer }) => {
  const [html, setHtml] = useState('');
  const [css, setCss] = useState('');
  const [isDataDeleted, toggleIsDataDeleted] = useState(true);
  const [isRenaming, toggleIsRenaming] = useState(true);

  const domWalk = (element, fn, dict) => {
    fn(element, dict);
    each(element.children, value => {
      domWalk(value, fn, dict);
    });
  };

  const sanitize = (html, css, isDataDeleted, areClassesAndIdsRenamed) => {
    let finalHtml = html;
    let finalCss = css;

    const parser = new DOMParser();
    const document_ = parser.parseFromString(html, 'text/html');
    const body = document_.getElementsByTagName('body')[0];
    const classesMap = {};
    const idsMap = {};

    const clearDataAttributes = element => {
      each(element.dataset, function(value, key) {
        // element.removeAttribute(`data-${key}`);
        delete element.dataset[key];
      });
    };

    const increaser = function*(index) {
      while (index < Number.MAX_SAFE_INTEGER) {
        yield index++;
      }
    };

    const iterator = increaser(0);

    const renameCssClasses = (element, map) => {
      const oldClasses = [];
      // const newClasses = []
      each(element.classList, value => {
        if (map[value] === undefined) {
          map[value] = `landing-c${iterator.next().value}`;
        }
        // newClasses.push(map[value])
        oldClasses.push(value);
      });
      each(oldClasses, value => {
        element.classList.replace(value, map[value]);
      });
    };

    const renameIds = (element, map) => {
      const { id } = element;

      if (!id) {
        return;
      }

      if (map[id] !== undefined) {
        element.removeAttribute('id');
        return;
      }

      map[id] = `landing-id${iterator.next().value}`;
      element.id = map[id];
    };

    if (isDataDeleted) {
      each(body.children, value => {
        domWalk(value, clearDataAttributes, null);
      });
    }

    if (areClassesAndIdsRenamed) {
      each(body.children, value => {
        domWalk(value, renameCssClasses, classesMap);
      });

      each(body.children, value => {
        domWalk(value, renameIds, idsMap);
      });

      const classesByLen = sortBy(keys(classesMap), k => {
        return -k.length;
      });

      each(classesByLen, value => {
        const re = new RegExp(`\\.${value}`, 'g');
        finalCss = finalCss.replace(re, `.${classesMap[value]}`);
      });

      const idsByLen = sortBy(keys(idsMap), k => {
        return -k.length;
      });

      each(idsByLen, value => {
        const re = new RegExp(`\\#${value}`, 'g');
        finalCss = finalCss.replace(re, `#${idsMap[value]}`);
      });
    }

    finalHtml = body.innerHTML;

    return [finalHtml, finalCss];
  };

  return (
    <Modal
      isOpen={isSanitizerOpen}
      onRequestClose={() => {
        toggleSanitizer(false);
      }}
      style={{
        top: '40%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        minHeight: '330px',
        minWidth: '750px'
      }}
    >
      <Header>Очиститель</Header>
      <div>HTML</div>
      <textarea value={html} onChange={e => setHtml(e.target.value)} />
      <div>CSS</div>
      <textarea value={css} onChange={e => setCss(e.target.value)} />
      <div>
        <Checkbox
          label="Удалять data атрибуты?"
          onChange={event => toggleIsDataDeleted(event.target.checked)}
          checked={isDataDeleted}
        />
      </div>
      <div>
        <Checkbox
          label="Переименовывать классы и id?"
          onChange={event => toggleIsRenaming(event.target.checked)}
          checked={isRenaming}
        />
      </div>
      <div>
        <Button
          text="Очистить"
          onClick={() => {
            const [finalHtml, finalCss] = sanitize(html, css, isDataDeleted, isRenaming);
            setHtml(finalHtml);
            setCss(finalCss);
          }}
        />
      </div>
    </Modal>
  );
};

export default SanitizerModal;
