import React from 'react';
import ReactModal from 'react-modal';

ReactModal.defaultStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    zIndex: 1000
  },
  content: {
    position: 'absolute',
    // top: '400px',
    // left: '400px',
    // right: '400px',
    // bottom: '400px',
    top: '40%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    border: '1px solid #ccc',
    background: '#fff',
    overflowY: 'auto',
    overflowX: 'hidden',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '4px',
    outline: 'none',
    padding: '20px',
    zIndex: 1000
  }
};

const Modal = ({ isOpen, children, onRequestClose, style }) => (
  <ReactModal
    onRequestClose={onRequestClose}
    isOpen={isOpen}
    ariaHideApp={false}
    style={{ content: style }}
  >
    {children}
  </ReactModal>
);

export default Modal;
