import React, { useState, useCallback } from 'react';
// import { useMutation } from 'react-apollo';
import styled from 'styled-components';
import { bind } from 'lodash';
import axios from 'axios';
// import { useDropzone } from 'react-dropzone';

import Button from 'components/Button';
import Modal from 'components/modals/Modal';
// import { UploadFromURLM } from 'queries/uploads';

const InputField = styled.input`
  font-size: 14px;
  width: 80%;
  height: 20px;
`;

const Label = styled.div`
  width: 100%;
  text-align: center;
  padding-bottom: 5px;
`;

const Header = styled.h3`
  padding-bottom: 10px;
  margin-top: 0px;
  width: 100%;
  text-align: center;
`;

const ButtonFlex = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

const OrDiv = styled.h3`
  width: 100%;
  text-align: center;
  padding: 20px 0px 30px 0px;
`;

const upload = (
  file,
  siteId,
  token,
  name,
  uploadCb,
  uploadCbParams,
  setIsUploading,
  setIsModalOpen
) => {
  const data = new FormData();
  data.append('siteId', siteId);
  data.append('name', name);
  data.append('file', file);

  axios
    .post('/upload-font', data, {
      headers: {
        Authorization: `JWT ${token}`,
        'Content-Type': 'multipart/form-data'
      }
    })
    .then(res => {
      setIsUploading(false);
      uploadCb(res.data.data[0], ...uploadCbParams);
      setIsModalOpen(false);
    })
    .catch(e => {
      console.log(e);
      setIsUploading(false);
    });
};

const UploadFontModal = ({
  isModalOpen,
  setIsModalOpen,
  siteId,
  uploadCb,
  uploadCbParams,
  token
}) => {
  // const [url, setUrl] = useState('');
  const [isUploading, setIsUploading] = useState(false);
  const [name, setName] = useState('');
  // const [uploadFromURL] = useMutation(UploadFromURLM, {
  //   onCompleted: data => {
  //     setIsUploading(false);
  //     uploadCb(data.uploadFromURL.url, ...uploadCbParams);
  //     setIsModalOpen(false);
  //   },
  //   onError: error => {
  //     setIsUploading(false);
  //   }
  // });
  // const onDrop = useCallback(acceptedFiles => {
  //   // console.log(acceptedFiles);
  //   const file = acceptedFiles[0];
  //   setIsUploading(true);
  //   upload(file, siteId, token, name, uploadCb, uploadCbParams, setIsUploading, setIsModalOpen);
  // }, []);
  // const { getRootProps, getInputProps, isDragActive } = useDropzone({
  //   onDrop,
  //   noClick: true
  // });
  const fileEl = React.createRef();
  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={() => {
        setIsModalOpen(false);
      }}
      style={{
        top: '40%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        minHeight: '130px',
        minWidth: '750px'
      }}
    >
      {/* <div {...getRootProps()}>
        {isDragActive ? (
          // {isDragged ? (
          <div
            style={{
              width: '100%',
              height: '330px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <div>Отпустите файл для загрузки</div>
          </div>
        ) : ( */}
      <>
        <Header>Загрузка шрифта</Header>
        {/* <Label>Загрузить по любой ссылке из интернета</Label>
            <ButtonFlex>
              <InputField
                name="uploadFromURL"
                label="URL:"
                value={url}
                placeholder="Вставьте ссылку сюда"
                onChange={event => {
                  setUrl(event.target.value);
                }}
              />
            </ButtonFlex>
            <ButtonFlex>
              <Button
                disabled={isUploading}
                text="Загрузить"
                onClick={() => {
                  setIsUploading(true);
                  uploadFromURL({ variables: { siteId, url } });
                }}
              />
            </ButtonFlex>
            <OrDiv>ИЛИ</OrDiv> */}
        <label htmlFor="filename" style={{ marginRight: '10px' }}>
          Название
        </label>
        <InputField
          name="filename"
          value={name}
          onChange={event => {
            setName(event.target.value);
          }}
        />
        <Label>Загрузите файл с компьютера, нажав на кнопку</Label>
        {/* accept влияет только на файлы, которые видит пользователь, нужна также проверка на сервере */}
        <input
          type="file"
          ref={fileEl}
          style={{ display: 'none' }}
          accept="font/woff, font/woff2"
          onChange={event => {
            setIsUploading(true);
            upload(
              event.target.files[0],
              siteId,
              token,
              name,
              uploadCb,
              uploadCbParams,
              setIsUploading,
              setIsModalOpen
            );
          }}
        />
        <ButtonFlex>
          <Button
            disabled={isUploading}
            text="Загрузить с компьютера"
            onClick={bind(() => {
              fileEl.current.click();
            }, this)}
          />
        </ButtonFlex>
      </>
      {/* )} */}
      {/* <input {...getInputProps({ multiple: false })} /> */}
      {/* </div> */}
    </Modal>
  );
};

export default UploadFontModal;
