import React, { useEffect, useRef } from 'react';
import { useMutation } from 'react-apollo';
import styled from 'styled-components';

import Button from 'components/Button';
import Modal from 'components/modals/Modal';
import { PublishPageM } from 'queries/publish';

const InputField = styled.input`
  font-size: 14px;
  width: 80%;
  height: 20px;
`;

const Header = styled.h3`
  padding-bottom: 10px;
  margin-top: 0px;
  width: 100%;
  text-align: center;
`;

const ButtonFlex = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const PublishPageModal = ({ isModalOpen, setIsModalOpen, pageId }) => {
  // const [prefix, setPrefix] = useState('');
  const [publishPage] = useMutation(PublishPageM, {
    onCompleted: data => {
      setIsModalOpen(false);
      window.open(`/generated-sites/${data.publishPage.site.urlPrefix}/`);
    },
    onError: error => {}
  });
  const prevState = usePrevious({ isModalOpen });

  useEffect(() => {
    if (isModalOpen && !prevState.isModalOpen) {
      publishPage({ variables: { pageId } });
    }
  });

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={() => {
        setIsModalOpen(false);
      }}
      style={{
        top: '40%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        minHeight: '130px',
        minWidth: '750px'
      }}
    >
      <>
        <Header>Опубликовать страницу</Header>
        {/* <label htmlFor="prefix" style={{ marginRight: '10px' }}>
          Префикс
        </label>
        <InputField
          name="prefix"
          value={prefix}
          onChange={event => {
            setPrefix(event.target.value);
          }}
        /> */}
        <ButtonFlex>
          {/* <Button
            text="Опубликовать"
            onClick={() => publishPage({ variables: { pageId, urlPrefix: prefix } })}
          /> */}
          Идет публикация...
        </ButtonFlex>
      </>
    </Modal>
  );
};

export default PublishPageModal;
