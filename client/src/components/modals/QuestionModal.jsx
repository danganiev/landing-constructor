import React from 'react';

import Modal from 'components/modals/Modal';
import Button from 'components/Button';

const QuestionModal = ({
  isOpen,
  onRequestClose,
  questionText,
  okText,
  okAction,
  cancelText,
  cancelAction,
  height
}) => {
  return (
    <Modal isOpen={isOpen} onRequestClose={onRequestClose} style={{ minHeight: height, height }}>
      <div>{questionText}</div>
      <Button text={okText} onClick={okAction} />
      <Button text={cancelText || 'Отмена'} onClick={cancelAction} />
    </Modal>
  );
};

export default QuestionModal;
