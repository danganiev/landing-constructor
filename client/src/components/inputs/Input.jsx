import React from 'react';
import styled from 'styled-components';

const InputField = styled.input`
  font-size: 14px;
`;

const Label = styled.label``;

const Input = ({ label, onChange, ...props }) => (
  <>
    <Label>{label}</Label>
    <InputField onChange={onChange} {...props} />
  </>
);

export default Input;
