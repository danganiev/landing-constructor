import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';

const SelectField = styled.select`
  height: 30px;
  padding: 5px;
  width: 300px;
  border: 2px solid #ccc;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  font-size: 14px;
  margin-bottom: 5px;
`;

const Label = styled.label`
  margin-bottom: 5px;
`;

export const Dropdown = ({ label, onChange, options, selected }) => (
  <>
    <Label>{label}</Label>
    <SelectField onChange={onChange} value={selected}>
      {_.map(options, (value, key) => (
        <option key={key} value={value}>
          {key}
        </option>
      ))}
    </SelectField>
  </>
);

export const ValueCbDropdown = ({ label, onChange, options, selected, valueCb }) => (
  <>
    <Label>{label}</Label>
    <SelectField onChange={onChange} value={selected}>
      {_.map(options, (value, key) => (
        <option key={key} value={valueCb(value, key)}>
          {key}
        </option>
      ))}
    </SelectField>
  </>
);
