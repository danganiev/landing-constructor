import React from 'react';
import styled from 'styled-components';

const TextareaField = styled.textarea``;

const Label = styled.label``;

const Textarea = ({ label, onChange, ...props }) => (
  <span>
    <Label>{label}</Label>
    <TextareaField onChange={onChange} {...props} />
  </span>
);

export default Textarea;
