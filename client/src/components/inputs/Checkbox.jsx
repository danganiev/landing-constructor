import React from 'react';
import styled from 'styled-components';

const InputField = styled.input``;

const Label = styled.label``;

const Checkbox = ({ label, onChange, ...props }) => (
  <>
    <Label>{label}</Label>
    <InputField type="checkbox" onChange={onChange} {...props} />
  </>
);

export default Checkbox;
