import React from 'react';
import { Field } from 'formik';
import styled from 'styled-components';

const Label = styled.label`
  margin-bottom: 5px;
`;

const Input = styled(({ isError, ...rest }) => <Field {...rest} />)`
  height: 30px;
  padding: 5px;
  width: 300px;
  border: 2px solid #ccc;
  border-color: ${props => (props.isError ? 'red' : '#ccc')};
  -webkit-border-radius: 5px;
  border-radius: 5px;
  font-size: 14px;
  margin-bottom: 5px

  &:focus {
    border-color: ${props => (props.isError ? 'red' : '#333')};
    outline: 0;
  }
`;

export default ({ label, ...props }) => {
  return (
    <>
      <Label>{label}</Label>
      <Input {...props} />
    </>
  );
};
