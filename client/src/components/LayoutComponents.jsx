// import React from 'react';
import styled from 'styled-components';

import constants from 'utils/constants';

// title in center
// const TitleInnerFlex = styled.div`
//   margin-top: 10px;
//   display: flex;
//   width: 600px;
//   justify-content: center;
//   align-items: center;
// `;

// const TitleOuterFlex = styled.div`
//   display: flex;
//   width: 100%;
//   justify-content: center;
// `;

export const Title = styled.div`
  font-size: 20px;
  padding-bottom: 10px;
`;

export const DivBlock = styled.div`
  background-color: ${constants.WHITE_GREY};
  height: 100%;
  border: 1px solid;
  border-color: rgb(190, 190, 190);
  border-radius: 3px;
`;

export const Content = styled.div`
  min-height: calc(100vh - 50px);
`;

export const InnerFlex = styled.div`
  margin-top: 10px;
  display: flex;
  width: 800px;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
`;

export const OuterFlex = styled.div`
  padding-top: 100px;
  display: flex;
  width: 100%;
  justify-content: center;
`;
