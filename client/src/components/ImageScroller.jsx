import React, { useState } from 'react';
import styled from 'styled-components';

const ImageScrollerDiv = styled.div.attrs(props => ({
  style: {
    backgroundPositionY: `${props.scrollY}%`
  }
}))`
  height: 300px;
  background-image: url(${props => props.url});
  background-size: cover;
  // cursor: zoom-in;
`;

const ImageScroller = () => {
  const url =
    'https://static.tildacdn.com/tild3430-3563-4465-b062-323238336264/tpl_bu_universal_0_r.jpg';
  const [isScrolling, setIsScrolling] = useState(false);
  const [scrollY, setScrollY] = useState(0);

  if (isScrolling) {
    setTimeout(() => {
      if (scrollY < 100) {
        let newScroll = scrollY + 0.33;
        if (newScroll > 100) {
          newScroll = 100;
        }
        setScrollY(newScroll);
      }
    }, 10);
  }

  return (
    <ImageScrollerDiv
      url={url}
      onMouseOver={() => {
        setIsScrolling(true);
      }}
      onMouseOut={() => {
        // setScrollY(0);
        setIsScrolling(false);
        setTimeout(() => setScrollY(0), 11);
      }}
      scrollY={scrollY}
    />
  );
};

export default ImageScroller;
