import styled from 'styled-components';

const ErrorBox = styled.div`
  color: red;
  font-size: 14px;
  margin-bottom: 10px;
  margin-top: 10px;
`;

export const AuthErrorBox = styled(ErrorBox)`
  margin-top: 0px;
`;

export default ErrorBox;
