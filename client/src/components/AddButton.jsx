import React from 'react';

const AddButton = ({ onClick }) => (
  <svg
    width="50px"
    height="50px"
    viewBox="0 0 50 50"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <title>Добавить элемент или блок</title>
    <desc>Created with Sketch.</desc>
    <g
      id="Editor"
      stroke="none"
      style={{ cursor: 'pointer' }}
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
      onClick={onClick}
    >
      <g id="Block-selected" transform="translate(-30.000000, -938.000000)">
        <g
          className="add-button-oval"
          id="Add-element-or-block"
          transform="translate(30.000000, 938.000000)"
        >
          <circle id="Oval" fill="#006FFF" cx="25" cy="25" r="25" />
          <rect
            id="Rectangle"
            fill="#FFFFFF"
            x="22.4166667"
            y="10.3333333"
            width="4.5"
            height="30"
          />
          <rect
            id="Rectangle-Copy"
            fill="#FFFFFF"
            transform="translate(24.666667, 25.333333) rotate(90.000000) translate(-24.666667, -25.333333) "
            x="22.4166667"
            y="10.3333333"
            width="4.5"
            height="30"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default AddButton;
