// @flow

import React from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

import constants from 'utils/constants';

const MyButton = styled.button`
  background-color: ${constants.BLUE}
  color: #fff;
  border: none;
  font-family: inherit;
  font-size: 14px;
  cursor: pointer;
  padding: 6px 10px;
  display: inline-block;
  margin: 10px 2.5px;
  letter-spacing: -0.5px;
  font-weight: 700;
  outline: none;
  position: relative;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
  border-radius: 3px;

  &:hover {
    background: ${constants.DARKER_BLUE};
  }

  &:after {
    content: '';
    position: absolute;
    z-index: -1;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    transition: all 0.3s;
  }

  :disabled{
    top: 0px;
    background: #78b3ff;
  }
`;

const Icon = styled.i`
  padding-right: ${props => (props.hasPadding ? '10px' : '0px')};
`;

const Button = ({ icon, text, ...props }) => (
  <MyButton {...props}>
    {icon ? (
      <span>
        <Icon className={`fas fa-${icon}`} hasPadding />
        {text}
      </span>
    ) : (
      text
    )}
  </MyButton>
);

const IconButton = withRouter(({ history, link, icon, ...props }) => (
  <MyButton
    onClick={() => {
      history.push(link);
    }}
    {...props}
  >
    <Icon className={`fas fa-${icon}`} />
  </MyButton>
));

const LinkedButton = withRouter(({ history, link, text, icon }) => (
  <Button
    onClick={() => {
      history.push(link);
    }}
    text={text}
    icon={icon}
  />
));

export default Button;

export { LinkedButton, IconButton };
