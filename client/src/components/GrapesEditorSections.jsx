// @flow

import React from 'react';
import { connect } from 'react-redux';

import { setEditor } from '../reducers/editor';
import { getEditorForSections } from '../grapesEditor';

class GrapesEditor extends React.Component {
  componentDidUpdate() {
    const { components } = this.props;
    if (components) {
      this.editor.setComponents(JSON.parse(components));
    }
  }

  componentDidMount() {
    const { dispatch, editorHTML, components, toolbar, token, sectionId } = this.props;

    this.editor = getEditorForSections(components, editorHTML, toolbar, token, sectionId);

    dispatch(setEditor(this.editor));
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    this.editor.destroy();
    this.editor = null;

    dispatch(setEditor(null));
  }

  render() {
    return <div id="gjs" ref={el => (this.el = el)} />;
  }
}

const mapStateToProps = state => ({
  toolbar: state.textEditor.toolbar,
  token: state.auth.token
});

export default connect(mapStateToProps)(GrapesEditor);
