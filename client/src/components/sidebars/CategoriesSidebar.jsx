/* eslint-disable react/prop-types */
// @flow

import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import _ from 'lodash';

import { Query } from 'react-apollo';

import { toggleSidebar } from 'reducers/editor';
import { CategoriesQ } from 'queries/sections';
import constants from 'utils/constants';

import ElementsSidebar from './ElementsSidebar';
import SectionsSidebar from './SectionsSidebar';

const CategoryMenu = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 100;
  width: 150px;
  height: 100%;
  background: ${constants.GREY};
  border-right: 1px ${constants.DARK_GREY} solid;
  box-shadow: 2px 0px 2px -1px rgba(0, 0, 0, 0.1);
`;

const CategoryMenuHeader = styled.h2`
  margin: 0;
  padding: 1em;
  color: rgba(0, 0, 0, 0.4);
  text-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
  font-weight: 300;
  font-size: 2em;
  height: 81px;
`;

const CategoryMenuList = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`;

const CategoryMenuElement = styled.li`
  :first-child .link {
    box-shadow: inset 0 -1px rgba(0, 0, 0, 0.2), inset 0 1px rgba(0, 0, 0, 0.2);
  }
`;

const SidebarContainer = styled.div`
  // height: 100%;
  z-index: 99;
  position: fixed;
  overflow: hidden;
  display: ${props => (props.toggled ? 'default' : 'none')};
`;

const CategoryMenuLink = styled.span`
  display: block;
  padding: 1em 1em 1em 1.2em;
  outline: none;
  box-shadow: inset 0 -1px rgba(0, 0, 0, 0.2);
  color: #f3efe0;
  color: #333333;
  text-shadow: 0 0 1px rgba(255, 255, 255, 0.1);
  letter-spacing: 1px;
  font-weight: 400;
  -webkit-transition: background 0.3s, box-shadow 0.3s;
  transition: background 0.3s, box-shadow 0.3s;
  text-decoration: none;

  :hover {
    background: rgba(0, 0, 0, 0.2);
    background-color: ${constants.BLUE}
    cursor: pointer
    box-shadow: inset 0 -1px rgba(0, 0, 0, 0);
    color: #fff;
  }
`;

const CloseLayer = styled.div`
  overflow: auto;
  position: fixed;
  z-index: 90;
  width: 100%;
  padding: 0;
  top: 0;
  background-color: #000;
  opacity: 0.01;
  height: 100%;
`;

const Categories = ({ dispatch, toggleSectionsSidebar }) => (
  <Query query={CategoriesQ}>
    {({ loading, error, data }) => {
      if (loading) return <span />;
      if (error) return <span>Произошла ошибка :(</span>;
      return (
        <span>
          {_.map(data.categories, category => (
            <CategoryMenuElement key={category.id}>
              <CategoryMenuLink
                onClick={() => {
                  toggleSectionsSidebar(true, category.id);
                }}
              >
                {category.name}
              </CategoryMenuLink>
            </CategoryMenuElement>
          ))}
        </span>
      );
    }}
  </Query>
);

class CategorySidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      elementsToggled: false,
      sectionsToggled: false,
      toggledCategory: null
    };

    this.toggleElementsSidebar = this.toggleElementsSidebar.bind(this);
    this.toggleSectionsSidebar = this.toggleSectionsSidebar.bind(this);
  }

  toggleElementsSidebar(value) {
    this.setState({ elementsToggled: value, sectionsToggled: false, toggledCategory: null });
  }

  toggleSectionsSidebar(value, categoryId) {
    this.setState({ elementsToggled: false, sectionsToggled: value, toggledCategory: categoryId });
  }

  render() {
    const { dispatch, sidebarToggled, editor } = this.props;
    const { elementsToggled, sectionsToggled, toggledCategory } = this.state;
    return (
      <>
        <SidebarContainer toggled={sidebarToggled}>
          <CategoryMenu>
            <CategoryMenuHeader />
            <CategoryMenuList>
              <CategoryMenuElement key="elements">
                <CategoryMenuLink className="link" onClick={() => this.toggleElementsSidebar(true)}>
                  <span style={{ fontWeight: 'bold' }}> Элементы</span>
                </CategoryMenuLink>
              </CategoryMenuElement>
              <Categories toggleSectionsSidebar={this.toggleSectionsSidebar} />
            </CategoryMenuList>
          </CategoryMenu>
          <ElementsSidebar
            editor={editor}
            dispatch={dispatch}
            sidebarToggled={sidebarToggled}
            toggled={elementsToggled}
          />
          <SectionsSidebar
            editor={editor}
            dispatch={dispatch}
            sidebarToggled={sidebarToggled}
            toggled={sectionsToggled}
            categoryId={toggledCategory}
          />
        </SidebarContainer>

        <CloseLayer
          style={{ display: sidebarToggled ? 'inherit' : 'none' }}
          onClick={() => dispatch(toggleSidebar())}
        />
      </>
    );
  }
}

const mapStateToProps = state => ({
  editor: state.editor.editor
});

export default connect(mapStateToProps)(CategorySidebar);
