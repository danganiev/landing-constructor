/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import styled from 'styled-components';
import { useQuery } from 'react-apollo';

import { toggleSidebar } from 'reducers/editor';

import { SectionsQ } from 'queries/sections';
import constants from 'utils/constants';

const SidebarContainer = styled.div`
  // height: 100%;
  z-index: 99;
  position: fixed;
  overflow: hidden;
  display: ${props => (props.toggled ? 'default' : 'none')};
`;

const SectionMenu = styled.div`
  overflow: auto;
  position: fixed;
  top: 0;
  left: 150px;
  z-index: 90;
  width: 450px;
  height: 100%;
  background: #e7e7e7;
  border-right: 1px ${constants.DARK_GREY} solid;
  box-shadow: 2px 0px 2px -1px rgba(0, 0, 0, 0.1);
`;

const SectionMenuHeader = styled.h2`
  margin: 0;
  padding: 1em;
  color: rgba(0, 0, 0, 0.4);
  text-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
  font-weight: 300;
  font-size: 2em;
  height: 81px;
`;

const SectionMenuList = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`;

const SectionMenuElement = styled.li`
  .sectionName {
    cursor: move;
    display: block;
    padding: 1em 1em 1em 1.2em;
    outline: none;
    box-shadow: inset 0 -1px rgba(0, 0, 0, 0.2);
    color: #333333;
    text-shadow: 0 0 1px rgba(255, 255, 255, 0.1);
    letter-spacing: 1px;
    font-weight: 400;
    -webkit-transition: background 0.3s, box-shadow 0.3s;
    transition: background 0.3s, box-shadow 0.3s;
    text-decoration: none;
  }

  &:first-child .sectionName {
    box-shadow: inset 0 -1px rgba(0, 0, 0, 0.2), inset 0 1px rgba(0, 0, 0, 0.2);
  }

  .sectionName:hover {
    background: ${constants.BLUE};
    box-shadow: inset 0 -1px rgba(0, 0, 0, 0);
    color: #fff;
  }
`;

const SectionImage = styled.img`
  border: 1px ${constants.BLACK_GREY} solid;
`;

const SectionsSidebar = ({ dispatch, sidebarToggled, toggled, editor, categoryId }) => {
  const { loading, error, data } = useQuery(SectionsQ, {
    variables: { categoryId },
    skip: !categoryId
  });

  if (loading || !categoryId) return null;
  if (error) return <p>Произошла ошибка :(</p>;
  return (
    <SidebarContainer toggled={sidebarToggled && toggled}>
      <SectionMenu>
        <SectionMenuHeader />
        <SectionMenuList>
          {data.sections.map(section => (
            <SectionMenuElement
              key={section.id}
              draggable="true"
              onDragStart={ev => {
                ev.dataTransfer.clearData();
                ev.dataTransfer.setData(
                  'text',
                  // '<form class="form"> <div class="form-group"> <label class="label">Name</label> <input placeholder="Type here your name" class="input"/> </div> <div class="form-group"> <label class="label">Email</label> <input type="email" placeholder="Type here your email" class="input"/> </div> <div class="form-group"> <label class="label">Gender</label> <input type="checkbox" class="checkbox" value="M"> <label class="checkbox-label">M</label> <input type="checkbox" class="checkbox" value="F"> <label class="checkbox-label">F</label> </div> <div class="form-group"> <label class="label">Message</label> <textarea class="textarea"></textarea> </div> <div class="form-group"> <button type="submit" class="button">Send</button> </div> </form>'
                  section.html
                );
                // нужен таймаут, иначе изображение под курсором драга не успеет сгенерироваться
                setTimeout(() => dispatch(toggleSidebar()), 100);
              }}
            >
              <span className="sectionName" href="">
                <div style={{ marginBottom: '5px' }}>{section.name}</div>
                <SectionImage src={section.previewUrl} width="400" height="300" alt="" />
              </span>
            </SectionMenuElement>
          ))}
        </SectionMenuList>
      </SectionMenu>
    </SidebarContainer>
  );
};

export default SectionsSidebar;
