import React from 'react';
import { ChromePicker } from 'react-color';
import styled from 'styled-components';
import constants from 'utils/constants';
import {
  makeTextItalic,
  makeTextBold,
  makeTextUnderlined,
  makeTextColored
} from '../../api/textEditor';
import { setToolbar } from '../../reducers/textEditor';
import { Header } from './Header';

const TextEditorHeader = styled(Header)`
  justify-content: start;
  // height: 500px;
`;

const TextEditorButton = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 24px;
  width: 75px;

  &:hover {
    background-color: ${constants.BLUE};
    color: white;
    cursor: pointer;
  }
`;

const Cover = styled.div`
  position: fixed;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
`;

const Popover = styled.div`
  position: absolute;
  left: 150px;
  top: 60px;
  z-index: 1001;
`;

class TextEditorToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.toolbarRef = React.createRef();
    this.setState = this.setState.bind(this);
    this.state = { displayPicker: false };
    props.dispatch(setToolbar(this.toolbarRef));
  }

  render() {
    const { textEditor, style, editor } = this.props;
    const { displayPicker } = this.state;
    const { setState } = this;
    return (
      <>
        <TextEditorHeader key="textToolbar" ref={this.toolbarRef} style={style}>
          <TextEditorButton
            title="Жирный"
            onClick={() => {
              makeTextBold(textEditor);
              // textEditor.focus();
              // window.textEditor = textEditor;
              // setTimeout(() => {
              document.getElementsByTagName('iframe')[0].contentWindow.focus();
              // }, 0);
            }}
          >
            <i className="fas fa-bold" />
          </TextEditorButton>
          <TextEditorButton
            title="Курсив"
            onClick={() => {
              makeTextItalic(textEditor);
              document.getElementsByTagName('iframe')[0].contentWindow.focus();
            }}
          >
            <span className="fas fa-italic" />
          </TextEditorButton>

          <TextEditorButton
            title="Подчеркивание"
            onClick={() => {
              makeTextUnderlined(textEditor);
              document.getElementsByTagName('iframe')[0].contentWindow.focus();
            }}
          >
            <i className="fas fa-underline" />
          </TextEditorButton>
          <TextEditorButton
            title="Цвет текста"
            onClick={() => {
              setState({ displayPicker: !displayPicker });
              editor.RichTextEditor.setPreventDisabling(!displayPicker);
            }}
          >
            <i className="fas fa-palette" />
          </TextEditorButton>
        </TextEditorHeader>
        {displayPicker ? (
          <Popover>
            <Cover
              onClick={() => {
                setState({ displayPicker: false });
                editor.RichTextEditor.setPreventDisabling(false);
              }}
            />
            <ChromePicker
              onChangeComplete={color => {
                makeTextColored(textEditor, color.hex);
                document.getElementsByTagName('iframe')[0].contentWindow.focus();
              }}
              disableAlpha
            />
          </Popover>
        ) : null}
      </>
    );
  }
}

export default TextEditorToolbar;
