/* eslint-disable max-classes-per-file */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/no-multi-comp */
import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import LogoWords from '../../images/logo-words.svg';
import { Header, HeaderLogo, HeaderMenu, HeaderLinkContainer, HeaderLink } from './Header';
import TextEditorToolbar from './TextEditorToolbar';

class MainToolbar extends React.Component {
  render() {
    const { savePage, editor, pageId, togglePreview, token } = this.props;
    return (
      <Header>
        <HeaderLogo>
          <NavLink to="/sites/">
            <img src={LogoWords} alt="Логотип Терции" />
          </NavLink>
        </HeaderLogo>
        <HeaderMenu>
          <HeaderLinkContainer
            onClick={() => {
              savePage();
            }}
          >
            <HeaderLink>Сохранить </HeaderLink>
          </HeaderLinkContainer>
          <HeaderLinkContainer
            onClick={() => {
              // togglePreview(true);
              // editor.on('previewStopped', () => {
              //   editor.off('previewStopped');
              //   togglePreview(false);
              // });
              // editor.Commands.run('preview');
              savePage();
              document.cookie = `jwtToken=${token};expires=never;path=/preview;samesite=strict;`;
              window.open(`/preview?pageId=${pageId}`, '_blank');
            }}
          >
            <HeaderLink>Предпросмотр</HeaderLink>
          </HeaderLinkContainer>
          <HeaderLinkContainer>
            <HeaderLink>Опубликовать</HeaderLink>
          </HeaderLinkContainer>
        </HeaderMenu>
      </Header>
    );
  }
}

class Toolbar extends React.Component {
  render() {
    const { isTextBeingEdited } = this.props;

    return (
      <div>
        <MainToolbar key="mainToolbar" {...this.props} />
        <TextEditorToolbar
          key="textEditorToolbar"
          {...this.props}
          style={{ display: isTextBeingEdited ? 'flex' : 'none' }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  editor: state.editor.editor,
  textEditor: state.textEditor.editor,
  isTextBeingEdited: state.textEditor.isTextBeingEdited,
  token: state.auth.token
});

export default connect(mapStateToProps)(Toolbar);
