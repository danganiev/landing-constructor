/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/no-multi-comp */
import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

// import I8N from '../../i8n/i8n';
import { Header, HeaderLink, HeaderLinkContainer, HeaderLogo, HeaderMenu } from './Header';
import TextEditorToolbar from './TextEditorToolbar';
import LogoWords from '../../images/logo-words.svg';

// const AdminMenu = styled.div`
const AdminMenu = styled(HeaderMenu)`
  flex: 0 1 300px;
`;

class MainToolbar extends React.Component {
  render() {
    // const { editor, saveAdminPage, page, updatePage } = this.props;
    const { editor, toggleModal } = this.props;
    return (
      <Header>
        <HeaderLogo>
          <NavLink to="/sites/">
            <img src={LogoWords} alt="Логотип Терции" />
          </NavLink>
        </HeaderLogo>
        <AdminMenu>
          {/* <label htmlFor="name" style={{ marginRight: '10px' }}>
            Название:
          </label>
          <input
            id="name"
            style={{ marginRight: '10px' }}
            value={page ? page.name : ''}
            onChange={event => {
              updatePage({ ...page, name: event.target.value });
            }}
          /> */}
          <HeaderLinkContainer
            onClick={() => {
              // const finalHtml = `${editor.getHtml()}<style>${editor.getCss()}</style>`;
              // saveAdminPage(page.id, page.name, finalHtml);
              toggleModal();
            }}
          >
            <HeaderLink>Сохранить </HeaderLink>
          </HeaderLinkContainer>
          <HeaderLinkContainer
            onClick={() => {
              editor.Commands.run('import-html');
            }}
          >
            <HeaderLink>Импорт HTML</HeaderLink>
          </HeaderLinkContainer>
        </AdminMenu>
      </Header>
    );
  }
}

class Toolbar extends React.Component {
  render() {
    const { isTextBeingEdited } = this.props;

    return (
      <div>
        <MainToolbar key="mainToolbar" {...this.props} />
        <TextEditorToolbar
          key="textEditorToolbar"
          {...this.props}
          style={{ display: isTextBeingEdited ? 'flex' : 'none' }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  editor: state.editor.editor,
  textEditor: state.textEditor.editor,
  isTextBeingEdited: state.textEditor.isTextBeingEdited
});

export default connect(mapStateToProps)(Toolbar);
