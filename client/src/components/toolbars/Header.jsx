import styled from 'styled-components';

import constants from 'utils/constants';

const Header = styled.header`
  position: fixed;
  height: ${constants.HEADER_HEIGHT};
  width: 100%;

  background: linear-gradient(0deg, ${constants.DARK_GREY}, ${constants.GREY});
  z-index: 1000;
  top: 0px;
  left: 0px;
  box-sizing: border-box;
  box-shadow: 0 0 5px rgba(170, 170, 170, 0.5);
  display: flex;
  justify-content: space-between;

  border-bottom: 1px solid;
  border-color: rgb(150, 150, 150);
`;

const HeaderLogo = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  padding: 0 1rem;
`;

const HeaderMenu = styled.div`
  flex: 0 1 400px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  height: 100%;
`;

const HeaderLinkContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  color: black;
  font-family: Helvetica, Memetica, sans-serif;
  font-weight: 100;

  &:hover {
    height: 100%;
    background-color: ${constants.BLUE};
    color: white;
  }
`;

const HeaderLink = styled.div`
  text-align: center;
  font-size: 14px;
  text-decoration: none;
  color: inherit;

  &:hover {
    color: white;
    cursor: pointer;
  }
`;

export { Header, HeaderLogo, HeaderMenu, HeaderLinkContainer, HeaderLink };
