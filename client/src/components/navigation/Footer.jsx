import React from 'react';
import styled from 'styled-components';

import constants from 'utils/constants';

const Footer = styled.footer`
  background-color: ${constants.BLACK_GREY};

  height: 50px;
  display: flex;
  justify-content: center;
  border-top: 1px solid rgb(170, 170, 170);
`;

const Link = styled.a`
  color: #fff;
`;

export default () => (
  <Footer>
    <Link href="/user-agreement/">Пользовательское соглашение</Link>
  </Footer>
);
