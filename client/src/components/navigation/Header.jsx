import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

// import { connect } from 'react-redux';

import constants from 'utils/constants';
import Logo from '../../images/logo.svg';

const Header = styled.header`
  position: fixed;
  height: ${constants.HEADER_HEIGHT};
  width: 100%;
  z-index: 10;
  background: linear-gradient(0deg, ${constants.DARK_GREY}, ${constants.GREY});
  top: 0px;
  left: 0px;
  box-sizing: border-box;
  box-shadow: 0 0 5px rgba(170, 170, 170, 1);
  display: flex;
  border-bottom: 1px solid;
  border-color: rgb(150, 150, 150);
`;

const HeaderLogo = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  padding: 0 1rem;
`;

const LeftMenu = styled.div`
  flex: 0 1 350px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  height: 100%;
`;

const RightMenu = styled(LeftMenu)`
  flex: 0 1 100px;
`;

const Inbetween = styled(LeftMenu)`
  flex: 0 1 calc(100% - 450px);
`;

const HeaderLinkContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  color: black;
  font-family: Helvetica, Memetica, sans-serif;
  font-weight: 100;

  &:hover {
    height: 100%;
    background-color: ${constants.BLUE};
    color: white;
  }
`.withComponent(NavLink);

const HeaderLink = styled.span`
  text-align: center;
  font-size: 14px;
  text-decoration: none;
  color: inherit;

  &:hover {
    color: white;
  }
`;

const CabinetToolbar = ({ editor, dispatch }) => (
  <Header>
    <HeaderLogo>
      <NavLink to="/sites/">
        <img src={Logo} alt="Logo" height="30px" width="30px" />
      </NavLink>
    </HeaderLogo>
    <LeftMenu>
      <HeaderLinkContainer to="/sites">
        <HeaderLink>Мои сайты </HeaderLink>
      </HeaderLinkContainer>
      <HeaderLinkContainer to="/tariff">
        <HeaderLink>Тарифы</HeaderLink>
      </HeaderLinkContainer>
      <HeaderLinkContainer to="/profile">
        <HeaderLink>Профиль</HeaderLink>
      </HeaderLinkContainer>
    </LeftMenu>
    <Inbetween />
    <RightMenu>
      <HeaderLinkContainer to="/logout">
        <HeaderLink>Выйти</HeaderLink>
      </HeaderLinkContainer>
    </RightMenu>
  </Header>
);

export default CabinetToolbar;
