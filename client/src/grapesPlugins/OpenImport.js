// import Backbone from 'backbone';
import $ from 'jquery';

export default (editor, config) => ({
  run(editor, sender, opts = {}) {
    sender && sender.set && sender.set('active', 0);
    const config = editor.getConfig();
    const modal = editor.Modal;
    const pfx = config.stylePrefix;
    this.cm = editor.CodeManager || null;

    if (!this.$editors) {
      const oHtmlEd = this.buildEditor('htmlmixed', 'hopscotch', 'HTML');
      const oCsslEd = this.buildEditor('css', 'hopscotch', 'CSS');
      this.htmlEditor = oHtmlEd.el;
      this.cssEditor = oCsslEd.el;
      const $editors = $(`<div class="${pfx}export-dl"></div>`);
      $editors.append(oHtmlEd.$el).append(oCsslEd.$el);
      this.$editors = $editors;
    }

    // Init import button
    const { htmlEditor, cssEditor } = this;
    const btnImp = document.createElement('button');
    btnImp.type = 'button';
    btnImp.innerHTML = 'Импортировать';
    btnImp.className = `${pfx}btn-prim ${pfx}btn-import`;
    btnImp.onclick = e => {
      // editor.setComponents(
      //   `${htmlEditor.editor.getValue().trim()}<style>${cssEditor.editor.getValue().trim()}</style>`
      // );
      editor.setComponents(htmlEditor.editor.getValue().trim());
      editor.setStyle(cssEditor.editor.getValue().trim());
      modal.close();
    };

    modal
      .open({
        title: 'Импорт HTML',
        content: this.$editors
      })
      .getModel()
      .once('change:open', () => editor.stopCommand(this.id));
    const html = editor.getHtml();
    let css = editor.getCss();
    css = css.replace('* { box-sizing: border-box; } body {margin: 0;}', '');
    css = css.replace(
      '*{box-sizing:border-box;}body{margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;}',
      ''
    );
    this.htmlEditor.setContent(html);
    this.cssEditor.setContent(css);
    modal.getContentEl().appendChild(btnImp);
  },

  stop(editor) {
    const modal = editor.Modal;
    modal && modal.close();
  },

  buildEditor(codeName, theme, label) {
    const input = document.createElement('textarea');
    !this.codeMirror && (this.codeMirror = this.cm.getViewer('CodeMirror'));

    const el = this.codeMirror.clone().set({
      label,
      codeName,
      theme,
      input,
      readOnly: false
    });

    const { $el } = new this.cm.EditorView({
      model: el,
      config: this.cm.getConfig()
    }).render();

    el.init(input);

    return { el, $el };
  }
});
