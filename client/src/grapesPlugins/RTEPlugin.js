import Quill from 'quill/quill';
import store from '../store';
import { setCurrentTextEditor } from '../reducers/textEditor';

export default (editor, opts = {}) => {
  // const options = {
  //   ...{
  //     options: {}
  //   },
  //   ...opts
  // };

  editor.setCustomRte({
    enable(el, rte) {
      const rteToolbar = editor.RichTextEditor.getToolbarEl();
      // Hide everything inside the GrapesJS' toolbar container
      [].forEach.call(rteToolbar.children, child => (child.style.display = 'none'));

      rte = new Quill(el, {
        debug: 'info',
        document: editor.Canvas.getFrameEl().contentDocument
      });

      this.focus(el, rte);
      store.dispatch(setCurrentTextEditor(rte));
      return rte;
    },

    disable(el, rte) {
      if (!rte) {
        return;
      }

      rte.blur();
      const editorContent = [];

      for (const child_ of rte.container.firstChild.children) {
        editorContent.push(child_);
      }

      rte.container.firstChild.remove();

      for (const child_ of editorContent) {
        rte.container.append(child_);
      }

      try {
        rte.container.removeChild(rte.container.getElementsByClassName('ql-clipboard')[0]);
      } catch {}
      rte = null;
      store.dispatch(setCurrentTextEditor(null));
    },
    focus(el, rte) {
      rte.focus();
    }
  });
};
