import gql from 'graphql-tag';

const ElementsQ = gql`
  query {
    elements {
      id
      name
      value
    }
  }
`;

const CategoriesQ = gql`
  query {
    categories {
      id
      name
    }
  }
`;

const SectionsQ = gql`
  query($categoryId: ID!) {
    sections(categoryId: $categoryId) {
      id
      name
      html
      sectionId
      previewUrl
      traitsJson
    }
  }
`;

const AllSectionsQ = gql`
  query {
    allSections {
      id
      name
      sectionId
    }
  }
`;

const TraitsQ = gql`
  query {
    allSections {
      id
      sectionId
      traitsJson
    }
  }
`;

const SectionQ = gql`
  query($id: ID!) {
    section(id: $id) {
      id
      name
      html
      traitsJson
      sectionId
      active
      description
    }
  }
`;

export { ElementsQ, SectionsQ, SectionQ, CategoriesQ, TraitsQ, AllSectionsQ };
