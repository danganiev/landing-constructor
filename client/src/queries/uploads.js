import gql from 'graphql-tag';

export const UploadFromURLM = gql`
  mutation($siteId: ID!, $url: String!) {
    uploadFromURL(siteId: $siteId, url: $url) {
      url
    }
  }
`;
