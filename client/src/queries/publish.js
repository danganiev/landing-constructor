import gql from 'graphql-tag';

export const PublishPageM = gql`
  mutation($pageId: ID!) {
    publishPage(pageId: $pageId) {
      id
      site {
        urlPrefix
      }
    }
  }
`;
