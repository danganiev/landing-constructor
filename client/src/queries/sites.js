import gql from 'graphql-tag';

export const SitesQ = gql`
  query {
    sites {
      id
      name
    }
  }
`;

export const SitePageQ = gql`
  query($id: ID!) {
    sitePage(id: $id) {
      id
      site {
        id
        jsonSettings
      }
      html
      css
    }
  }
`;

export const SiteWithPagesQ = gql`
  query($id: ID!) {
    site(id: $id) {
      id
      name
      pages {
        id
        name
        html
        css
      }
    }
  }
`;

export const PageTemplateQ = gql`
  query($id: ID!) {
    pageTemplate(id: $id) {
      id
      html
      name
      css
    }
  }
`;

export const PageTemplatesQ = gql`
  query {
    pageTemplates {
      id
      name
      description
      previewUrl
    }
  }
`;

export const UpdateSitePageM = gql`
  mutation UpdateSitePage($id: ID!, $html: String, $css: String) {
    updateSitePage(id: $id, html: $html, css: $css) {
      id
      html
    }
  }
`;

export const CreateSiteFromPageTemplateM = gql`
  mutation($name: String!, $templateId: ID!) {
    createSiteFromPageTemplate(name: $name, templateId: $templateId) {
      id
      pages {
        id
      }
    }
  }
`;

export const CreatePageFromTemplateM = gql`
  mutation($siteId: ID!, $templateId: ID!) {
    createPageFromTemplate(siteId: $siteId, templateId: $templateId) {
      id
    }
  }
`;
// export const GenerateWebsiteQ = ({ siteId }) => gql`
//   query {
//     generateWebsite(siteId: "${siteId}") {
//       uuid
//       id
//     }
//   }
// `;

export const DeleteSiteM = gql`
  mutation($id: ID!) {
    deleteSite(id: $id) {
      result
    }
  }
`;

export const DeleteSitePageM = gql`
  mutation($id: ID!) {
    deleteSitePage(id: $id) {
      result
    }
  }
`;
