import gql from 'graphql-tag';

export const SettingsQ = gql`
  query($id: ID!) {
    site(id: $id) {
      id
      name
      jsonSettings
    }
  }
`;

export const UpdateSettingsM = gql`
  mutation($id: ID!, $name: String, $jsonSettings: String) {
    updateSite(id: $id, name: $name, jsonSettings: $jsonSettings) {
      id
      name
      jsonSettings
    }
  }
`;

export const SiteFontsQ = gql`
  query($siteId: ID!) {
    siteFonts(siteId: $siteId) {
      id
      name
      file {
        url
      }
    }
  }
`;

export const DeleteSiteFileM = gql`
  mutation($siteFileId: ID!) {
    deleteSiteFile(siteFileId: $siteFileId) {
      result
    }
  }
`;

export const PageSettingsQ = gql`
  query($id: ID!) {
    sitePage(id: $id) {
      id
      name
      jsonSettings
    }
  }
`;

export const UpdatePageSettingsM = gql`
  mutation($id: ID!, $name: String, $jsonSettings: String) {
    updateSitePage(id: $id, name: $name, jsonSettings: $jsonSettings) {
      id
      name
      jsonSettings
    }
  }
`;
