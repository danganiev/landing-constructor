import gql from 'graphql-tag';

export const TokenAuthQ = gql`
  query($username: String!, $password: String!) {
    tokenAuth(username: $username, password: $password) {
      token
    }
  }
`;

export const UserProfileQ = gql`
  query {
    user {
      name
      email
    }
  }
`;

export const UserPermissionsQ = gql`
  query {
    user {
      permissions {
        name
      }
    }
  }
`;

export const CreateUserM = gql`
  mutation($email: String!, $password: String!, $name: String) {
    createUser(email: $email, password: $password, name: $name) {
      token
    }
  }
`;

export const UpdateUserM = gql`
  mutation($email: String!, $name: String!) {
    updateUser(data: { email: $email, name: $name }) {
      id
      name
      email
    }
  }
`;

export const UpdatePasswordM = gql`
  mutation($oldPassword: String!, $newPassword: String!) {
    updatePassword(oldPassword: $oldPassword, newPassword: $newPassword) {
      result
    }
  }
`;

export const RestorePasswordM = gql`
  mutation($email: String!) {
    restorePassword(email: $email) {
      result
    }
  }
`;
