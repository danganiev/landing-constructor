import gql from 'graphql-tag';

const UpsertAdminSectionM = gql`
  mutation(
    $id: ID
    $name: String!
    $html: String!
    $traits: String
    $sectionId: String
    $active: Boolean!
    $description: String
  ) {
    upsertAdminSection(
      id: $id
      name: $name
      html: $html
      traitsJson: $traits
      sectionId: $sectionId
      description: $description
      active: $active
    ) {
      id
      name
      html
      traitsJson
      sectionId
      active
      description
    }
  }
`;

const UpsertAdminPageM = gql`
  mutation($id: ID, $name: String!, $html: String!) {
    upsertAdminPage(id: $id, name: $name, html: $html) {
      id
      name
      html
    }
  }
`;

const UpsertPageTemplateM = gql`
  mutation($id: ID, $name: String!, $html: String!, $css: String!) {
    upsertPageTemplate(id: $id, name: $name, html: $html, css: $css) {
      id
      name
      html
      css
    }
  }
`;

export { UpsertAdminSectionM, UpsertAdminPageM, UpsertPageTemplateM };
