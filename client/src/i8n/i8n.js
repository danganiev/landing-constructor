// This is a i8n mockup, since I'll be using only russian language for now
class I8N {
  constructor(language) {
    this.language = language;

    this.json = require('./translations/ru-RU.json');
  }

  // gets a translation value for a key for selected language
  getValue(key) {
    return this.json[key] || '';
  }
}

const defaultLanguage = new I8N('ru-RU');
export default defaultLanguage;
