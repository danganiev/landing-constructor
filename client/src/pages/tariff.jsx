import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import { Header, Footer } from '../components/navigation';
import { Content } from '../components/LayoutComponents';
import Button from '../components/Button';

const Tariff = () => (
  <div>
    <Content>
      <Header selected="tariff" />
      <Query
        query={gql`
          query {
            sites {
              id
              name
            }
          }
        `}
      >
        {({ loading, error, data }) => {
          if (loading) return <p />;
          if (error) return <p>Произошла ошибка :(</p>;

          return <div />;
        }}
      </Query>
    </Content>
    <Footer />
  </div>
);

export default connect()(Tariff);
