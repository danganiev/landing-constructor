// @flow

import React from 'react';
import { connect } from 'react-redux';

import { logout } from 'reducers/auth';

class Logout extends React.Component {
  componentDidMount() {
    const { dispatch, history } = this.props;

    logout(dispatch, history);
  }

  render() {
    return <div />;
  }
}

export default connect()(Logout);
