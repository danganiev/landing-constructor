// @flow

import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Formik, Form, ErrorMessage } from 'formik';
// import { string, object } from 'yup';
import { each } from 'lodash';

import Button from 'components/Button';
import { DivBlock } from 'components/LayoutComponents';
import { AuthErrorBox } from 'components/ErrorBox';
import {
  ParentContainer,
  DivBlockInside,
  LogoDiv,
  FormDiv,
  AuthInput,
  LinkDiv,
  AuthLink
} from 'components/AuthComponents';
import { updateToken } from 'reducers/auth';
import apolloClient from 'utils/apolloClient';
import { RestorePasswordM } from 'queries/users';
import LogoWords from 'images/logo-words.svg';

const FormContainer = styled.div``;

class RestorePassword extends React.Component {
  render() {
    const { history, dispatch } = this.props;
    return (
      <ParentContainer>
        <FormContainer>
          <DivBlock>
            <DivBlockInside>
              <LogoDiv>
                <NavLink to="/">
                  <img src={LogoWords} alt="Логотип Терции" />
                </NavLink>
              </LogoDiv>

              <FormDiv>
                <Formik
                  validateOnBlur={false}
                  validateOnChange={false}
                  initialValues={{ email: '', password: '' }}
                  onSubmit={(values, actions) => {
                    actions.setSubmitting(true);

                    apolloClient
                      .mutate({
                        mutation: RestorePasswordM,
                        variables: {
                          email: values.email
                        }
                      })
                      .then(result => {
                        actions.setSubmitting(false);
                        dispatch(updateToken(result.data.tokenAuth));
                        history.push('/');
                      })
                      .catch(e => {
                        each(e.graphQLErrors, (value, index) => {
                          if (value.extensions.code === 'SERVER_VALIDATION_ERROR') {
                            actions.setErrors(value.extensions.exception.errors);
                          }
                        });
                        actions.setSubmitting(false);
                      });
                  }}
                  validate={values => {
                    const errors = {};

                    if (!values.email) {
                      errors.email = 'Пожалуйста введите электронную почту';
                    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
                      errors.email = 'Неверный e-mail';
                    }
                    return errors;
                  }}
                >
                  {({ touched, errors, values }) => (
                    <Form>
                      <div
                        style={{
                          width: '250px',
                          paddingBottom: '30px',
                          paddingLeft: '25px',
                          fontSize: '14px',
                          fontWeight: 'bold',
                          textAlign: 'center'
                        }}
                      >
                        Восстановление пароля
                      </div>
                      <div
                        style={{
                          width: '250px',
                          paddingBottom: '15px',
                          paddingLeft: '25px',
                          fontSize: '14px',
                          textAlign: 'center'
                        }}
                      >
                        Введите адрес электронной почты, указанный при регистрации
                      </div>
                      <AuthInput
                        name="email"
                        value={values.email}
                        isError={touched.email && errors.email}
                      />
                      <ErrorMessage component={AuthErrorBox} name="email" />
                      <div
                        style={{
                          display: 'flex',
                          width: '300px',
                          justifyContent: 'center',
                          paddingBottom: '10px'
                        }}
                      >
                        <Button text="Восстановить" type="submit" />
                      </div>
                      <LinkDiv>
                        <AuthLink to="/login">Вспомнили пароль?</AuthLink>
                        <AuthLink to="/registration">Регистрация</AuthLink>
                      </LinkDiv>
                    </Form>
                  )}
                </Formik>
              </FormDiv>
            </DivBlockInside>
          </DivBlock>
        </FormContainer>
      </ParentContainer>
    );
  }
}

export default connect()(RestorePassword);
