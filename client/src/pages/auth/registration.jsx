// @flow

import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Formik, Form, ErrorMessage } from 'formik';
import { string, object } from 'yup';
import { each } from 'lodash';

import Button from 'components/Button';
import { DivBlock } from 'components/LayoutComponents';
import { AuthErrorBox } from 'components/ErrorBox';
import {
  ParentContainer,
  DivBlockInside,
  LogoDiv,
  FormDiv,
  AuthInput,
  LinkDiv,
  AuthLink
} from 'components/AuthComponents';
import { updateToken } from 'reducers/auth';
import apolloClient from 'utils/apolloClient';
import { CreateUserM } from 'queries/users';
import LogoWords from 'images/logo-words.svg';

const FormContainer = styled.div``;

class Login extends React.Component {
  render() {
    const { history, dispatch } = this.props;
    return (
      <ParentContainer>
        <FormContainer>
          <DivBlock>
            <DivBlockInside>
              <LogoDiv>
                <NavLink to="/">
                  <img src={LogoWords} alt="Логотип Терции" />
                </NavLink>
              </LogoDiv>
              <FormDiv>
                <Formik
                  validateOnBlur={false}
                  initialValues={{ email: '', password: '', _name: '' }}
                  onSubmit={(values, actions) => {
                    actions.setSubmitting(true);

                    apolloClient
                      .mutate({
                        mutation: CreateUserM,
                        variables: {
                          email: values.email,
                          password: values.password,
                          name: values._name
                        }
                      })
                      .then(result => {
                        actions.setSubmitting(false);
                        dispatch(updateToken(result.data.createUser));
                        history.push('/sites');
                      })
                      .catch(e => {
                        each(e.graphQLErrors, (value, index) => {
                          if (value.extensions.code === 'SERVER_VALIDATION_ERROR') {
                            actions.setErrors(value.extensions.exception.errors);
                          }
                        });
                        actions.setSubmitting(false);
                      });
                  }}
                  validate={values => {
                    const errors = {};

                    if (!values.email) {
                      errors.email = 'Пожалуйста введите электронную почту';
                    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
                      errors.email = 'Неверный e-mail';
                    }
                    return errors;
                  }}
                  validationSchema={object().shape({
                    email: string().required('Пожалуйста введите электронную почту'),
                    password: string().required('Пожалуйста введите пароль')
                  })}
                >
                  {({ touched, errors, values }) => (
                    <Form>
                      <div
                        style={{
                          width: '300px',
                          paddingBottom: '30px',
                          fontSize: '14px',
                          fontWeight: 'bold'
                        }}
                      >
                        Регистрация — быстрая и бесплатная
                      </div>
                      <AuthInput
                        label="Как к вам обращаться?"
                        name="_name"
                        value={values._name}
                        isError={touched._name && errors._name}
                      />
                      <ErrorMessage component={AuthErrorBox} name="_name" />
                      <AuthInput
                        label="Электронная почта"
                        name="email"
                        value={values.email}
                        isError={touched.email && errors.email}
                      />
                      <ErrorMessage component={AuthErrorBox} name="email" />
                      <AuthInput
                        label="Пароль"
                        value={values.password}
                        name="password"
                        type="password"
                        isError={touched.password && errors.password}
                      />
                      <ErrorMessage component={AuthErrorBox} name="password" />
                      <Button text="Зарегистрироваться" type="submit" />
                      <div
                        style={{
                          width: '300px',
                          padding: '10px 0px',
                          fontSize: '12px'
                        }}
                      >
                        Нажимая на кнопку «Зарегистрироваться», вы принимаете условия
                        <AuthLink to="/user-agreement"> пользовательского соглашения</AuthLink>
                      </div>
                      <LinkDiv>
                        <AuthLink to="/login">Уже зарегистрированы? Войти</AuthLink>
                      </LinkDiv>
                    </Form>
                  )}
                </Formik>
              </FormDiv>
            </DivBlockInside>
          </DivBlock>
        </FormContainer>
      </ParentContainer>
    );
  }
}

export default connect()(Login);
