// @flow

import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Formik, Form, ErrorMessage } from 'formik';
import { string, object } from 'yup';
import { each } from 'lodash';

import Button from 'components/Button';
import { DivBlock } from 'components/LayoutComponents';
import { AuthErrorBox } from 'components/ErrorBox';
import {
  ParentContainer,
  DivBlockInside,
  LogoDiv,
  FormDiv,
  AuthInput,
  LinkDiv,
  AuthLink
} from 'components/AuthComponents';
import { updateToken } from 'reducers/auth';
import apolloClient from 'utils/apolloClient';
import { TokenAuthQ } from 'queries/users';
import LogoWords from 'images/logo-words.svg';

const FormContainer = styled.div``;

class Login extends React.Component {
  render() {
    const { history, dispatch } = this.props;
    return (
      <ParentContainer>
        <FormContainer>
          <DivBlock>
            <DivBlockInside>
              <LogoDiv>
                <NavLink to="/">
                  <img src={LogoWords} alt="Логотип Терции" />
                </NavLink>
              </LogoDiv>
              <FormDiv>
                <Formik
                  validateOnBlur={false}
                  initialValues={{ username: '', password: '' }}
                  onSubmit={(values, actions) => {
                    actions.setSubmitting(true);

                    apolloClient
                      .query({
                        query: TokenAuthQ,
                        variables: {
                          username: values.username,
                          password: values.password
                        }
                      })
                      .then(result => {
                        actions.setSubmitting(false);
                        dispatch(updateToken(result.data.tokenAuth));
                        history.push('/');
                      })
                      .catch(e => {
                        each(e.graphQLErrors, (value, index) => {
                          if (value.extensions.code === 'SERVER_VALIDATION_ERROR') {
                            actions.setErrors(value.extensions.exception.errors);
                          }
                        });
                        actions.setSubmitting(false);
                      });
                  }}
                  validate={values => {
                    const errors = {};

                    return errors;
                  }}
                  validationSchema={object().shape({
                    username: string().required('Введите электронную почту'),
                    password: string().required('Введите пароль')
                  })}
                >
                  {({ touched, errors, values }) => (
                    <Form>
                      <AuthInput
                        label="Электронная почта"
                        name="username"
                        value={values.username}
                        isError={touched.username && errors.username}
                      />
                      <ErrorMessage component={AuthErrorBox} name="username" />
                      <AuthInput
                        label="Пароль"
                        value={values.password}
                        name="password"
                        type="password"
                        isError={touched.password && errors.password}
                      />
                      <ErrorMessage component={AuthErrorBox} name="password" />
                      <Button text="Войти" type="submit" />
                      <LinkDiv>
                        <AuthLink to="/restore-password">Забыли пароль?</AuthLink>
                        <AuthLink to="/registration">Регистрация</AuthLink>
                      </LinkDiv>
                    </Form>
                  )}
                </Formik>
              </FormDiv>
            </DivBlockInside>
          </DivBlock>
        </FormContainer>
      </ParentContainer>
    );
  }
}

export default connect()(Login);
