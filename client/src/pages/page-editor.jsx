/* eslint-disable react/prop-types */
// @flow
import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { each } from 'lodash';
import Noty from 'noty';

import constants from 'utils/constants';
import CategoriesSidebar from 'components/sidebars/CategoriesSidebar';
import TraitsSidebar from 'components/TraitsSidebar';
import SiteEditorToolbar from 'components/toolbars/SiteEditorToolbar';
import GrapesEditor from 'components/GrapesEditor';
import AddButton from 'components/AddButton';
import { toggleSidebar, updateSectionsAndTraits, toggleComponentSettings } from 'reducers/editor';

import apolloClient from 'utils/apolloClient';
import { SitePageQ, UpdateSitePageM } from 'queries/sites';
import { TraitsQ } from 'queries/sections';

const MainContainer = styled.div`
  height: 100%;
`;

const AddButtonContainer = styled.div`
  position: fixed;
  bottom: 30px;
  left: 30px;
  z-index: 1000;
`;

// function startWebsiteGeneration() {
//   apolloClient
//     .query({
//       query: GenerateWebsiteQ({ siteId: 'cjt759j4y001m07754m55res2' })
//     })
//     .then(result => {
//       console.log(result);
//     });
// }

class PageEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      page: null,
      previewToggled: false,
      settings: {}
    };

    this.togglePreview = this.togglePreview.bind(this);
    this.setState = this.setState.bind(this);
    this.savePage = this.savePage.bind(this);
  }

  componentDidMount() {
    const { match, dispatch } = this.props;

    apolloClient
      .query({
        query: SitePageQ,
        variables: {
          id: match.params.pageId
        }
      })
      .then(result => {
        if (result.data) {
          this.setState({
            page: result.data.sitePage,
            settings: JSON.parse(result.data.sitePage.site.jsonSettings)
          });
        }
      });

    apolloClient
      .query({
        query: TraitsQ
      })
      .then(result => {
        if (result.data) {
          const { data } = result;
          const sectionsAndTraits = {};
          each(data.allSections, value => {
            sectionsAndTraits[value.sectionId] = JSON.parse(value.traitsJson);
          });

          dispatch(updateSectionsAndTraits(sectionsAndTraits));
        }
      });

    document.body.style.overflow = 'hidden';
  }

  componentWillUnmount() {
    document.body.style.overflow = 'auto';
  }

  savePage(silent) {
    const { editor } = this.props;
    const { page } = this.state;

    // console.log(editor.getHtml());
    // console.log('----------');
    // console.log(editor.getCss());
    apolloClient
      .mutate({
        mutation: UpdateSitePageM,
        variables: {
          id: page.id,
          html: editor.getHtml(),
          css: editor.getCss()
        }
      })
      .then(result => {
        if (silent) {
          return;
        }
        new Noty({
          text: 'Изменения сохранены',
          type: 'success'
        }).show();
      });
  }

  togglePreview(value) {
    this.setState({ previewToggled: value });
  }

  render() {
    const { categories, selectedCategory, dispatch, match, editor, isToggled } = this.props;
    const { page, previewToggled, settings } = this.state;
    const { savePage } = this;

    if (page === null || settings === null) {
      return <div style={{ margin: '20px' }}>Загружаем страницу...</div>;
    }

    return (
      <MainContainer>
        {previewToggled ? null : (
          <SiteEditorToolbar
            savePage={savePage}
            editor={editor}
            pageId={page.id}
            togglePreview={this.togglePreview}
          />
        )}
        <div style={{ height: '100%', zIndex: 100 }}>
          <CategoriesSidebar
            sidebarToggled={isToggled}
            categories={categories}
            selectedCategory={selectedCategory}
          />
          {/* Пока передаем сайт на всякий случай, если в будущем он будет нужен */}
          <TraitsSidebar site={{ id: match.params.siteId }} savePage={savePage} pageId={page.id} />
          {/* <TraitsSidebar site={{ id: match.params.siteId }} savePage={savePage} /> */}
          <div
            id="editor-container"
            style={{
              paddingTop: previewToggled ? '0px' : constants.HEADER_HEIGHT,
              height: 'calc(100% - 60px)'
            }}
          >
            {page !== null && (
              <GrapesEditor
                editorHTML={`<style>${page.css}${settings.bodyFontCss ? settings.bodyFontCss : ''}${
                  settings.headerFontCss ? settings.headerFontCss : ''
                }</style>${page.html}`}
                siteId={match.params.siteId}
                saveFn={savePage}
              />
            )}
          </div>
        </div>
        {previewToggled ? null : (
          <AddButtonContainer>
            <AddButton
              onClick={e => {
                e.preventDefault();
                e.stopPropagation();
                e.nativeEvent.stopImmediatePropagation();
                dispatch(toggleComponentSettings(false));
                dispatch(toggleSidebar());
              }}
            />
          </AddButtonContainer>
        )}
      </MainContainer>
    );
  }
}

const mapStateToProps = state => ({
  isToggled: state.editor.isToggled,
  editor: state.editor.editor,
  textEditor: state.textEditor.editor
});

export default connect(mapStateToProps)(PageEditor);
