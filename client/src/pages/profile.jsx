// @flow
import React, { useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { useQuery } from 'react-apollo';
import Noty from 'noty';

import { string, object } from 'yup';
import { Formik, Form, ErrorMessage } from 'formik';

import { Header, Footer } from 'components/navigation';
import { Content, InnerFlex, OuterFlex, DivBlock, Title } from 'components/LayoutComponents';
import Button from 'components/Button';
import ErrorBox from 'components/ErrorBox';
import FormikInput from 'components/inputs/FormikInput';
import { Menu, MenuView, Tab } from 'components/TabMenu';
import { UpdateUserM, UserProfileQ, UpdatePasswordM } from 'queries/users';
import onSubmit from 'utils/formSubmitter';

const ProfileDiv = styled(DivBlock)`
  display: flex;
  height: 500px;
  width: 100%;
`;

const Flex = styled.div`
  margin-top: 10px;
  display: flex;
  width: 100%;
  justify-content: center;
`;

const Grid = styled.div`
  width: 600px;
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 10px;
`;

const ProfileInputBox = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`;

const ProfileBlock = () => {
  const { loading, error, data } = useQuery(UserProfileQ, {});

  if (loading) return null;
  if (error) return <p>Произошла ошибка :(</p>;
  return (
    <Tab>
      <Flex>
        <Grid>
          <Formik
            initialValues={{ name: data.user.name, email: data.user.email }}
            onSubmit={(values, actions) => {
              onSubmit(
                actions,
                UpdateUserM,
                {
                  name: values.name,
                  email: values.email
                },
                () => {
                  new Noty({
                    text: 'Изменения сохранены',
                    type: 'success'
                  }).show();
                }
              );
            }}
            validate={values => {
              const errors = {};

              if (!values.email) {
                errors.email = 'Обязательное поле';
              } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
                errors.email = 'Неверный e-mail';
              }

              return errors;
            }}
            validationSchema={object().shape({
              name: string().required('Обязательное поле')
            })}
          >
            {({ errors, values }) => (
              <Form>
                <ProfileInputBox>
                  <FormikInput
                    label="Имя"
                    name="name"
                    isError={errors.name !== undefined}
                    value={values.name}
                  />
                  <ErrorMessage component={ErrorBox} name="name" />
                </ProfileInputBox>
                <ProfileInputBox>
                  <FormikInput
                    label="Электронная почта"
                    name="email"
                    isError={errors.email !== undefined}
                    value={values.email}
                  />
                  <ErrorMessage component={ErrorBox} name="email" />
                </ProfileInputBox>
                <Button text="Сохранить" type="submit" />
              </Form>
            )}
          </Formik>
        </Grid>
      </Flex>
    </Tab>
  );
};

const PasswordTab = () => {
  return (
    <Tab>
      <Flex>
        <Grid>
          <Formik
            initialValues={{ password: '', newPassword: '', newPasswordAgain: '' }}
            onSubmit={(values, actions) => {
              onSubmit(
                actions,
                UpdatePasswordM,
                {
                  oldPassword: values.password,
                  newPassword: values.newPassword
                },
                () => {
                  new Noty({
                    text: 'Пароль изменен',
                    type: 'success'
                  }).show();
                }
              );
            }}
            validate={values => {
              const errors = {};

              if (values.newPassword !== values.newPasswordAgain) {
                errors.newPassword = 'Пароли не совпадают';
                errors.newPasswordAgain = 'Пароли не совпадают';
              }
              return errors;
            }}
            validationSchema={object().shape({
              password: string().required('Введите текущий пароль'),
              newPassword: string().required('Пароль не может быть пустым')
            })}
          >
            {({ touched, errors, values }) => (
              <Form>
                <ProfileInputBox>
                  <FormikInput
                    label="Текущий пароль"
                    name="password"
                    isError={touched.password && errors.password}
                    value={values.password}
                    type="password"
                  />
                  <ErrorMessage component={ErrorBox} name="password" />
                </ProfileInputBox>
                <ProfileInputBox>
                  <FormikInput
                    label="Новый пароль"
                    name="newPassword"
                    isError={touched.newPassword && errors.newPassword}
                    value={values.newPassword}
                    type="password"
                  />
                  <ErrorMessage component={ErrorBox} name="newPassword" />
                </ProfileInputBox>
                <ProfileInputBox>
                  <FormikInput
                    label="Повтор нового пароля"
                    name="newPasswordAgain"
                    isError={touched.newPasswordAgain && errors.newPasswordAgain !== undefined}
                    value={values.newPasswordAgain}
                    type="password"
                  />
                  <ErrorMessage component={ErrorBox} name="newPasswordAgain" />
                </ProfileInputBox>
                <Button text="Сменить пароль" type="submit" />
              </Form>
            )}
          </Formik>
        </Grid>
      </Flex>
    </Tab>
  );
};

const ComponentsMap = {};

ComponentsMap[ProfileBlock.name] = ProfileBlock;
ComponentsMap[PasswordTab.name] = PasswordTab;

const Profile = ({ settings, match, history }) => {
  const [selectedTab, setSelectedTab] = useState('ProfileBlock');

  return (
    <>
      <Content>
        <Header selected="profile" />
        <OuterFlex>
          <InnerFlex>
            <Title>Профиль</Title>
            <ProfileDiv>
              <Menu
                setSelectedTab={setSelectedTab}
                links={{
                  ProfileBlock: 'Данные пользователя',
                  PasswordTab: 'Смена пароля'
                }}
              />
              <MenuView componentsMap={ComponentsMap} component={selectedTab} />
            </ProfileDiv>
          </InnerFlex>
        </OuterFlex>
      </Content>
      <Footer />
    </>
  );
};

export default connect()(Profile);
