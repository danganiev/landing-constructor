/* eslint-disable max-classes-per-file */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/prefer-stateless-function */
import React, { useState } from 'react';
import styled from 'styled-components';
import { Query, useMutation, useQuery } from 'react-apollo';
import { Formik, Form, ErrorMessage } from 'formik';
import Noty from 'noty';
import { cloneDeep } from 'lodash';

import QuestionModal from 'components/modals/QuestionModal';
import { Header, Footer } from 'components/navigation';
import { Content, InnerFlex, OuterFlex, DivBlock, Title } from 'components/LayoutComponents';
import Button from 'components/Button';
import ErrorBox from 'components/ErrorBox';
import FormikInput from 'components/inputs/FormikInput';
import { Menu, Tab, MenuView } from 'components/TabMenu';

import onSubmit from 'utils/formSubmitter';
import { UpdatePageSettingsM, PageSettingsQ } from 'queries/site-settings';
import { DeleteSitePageM } from 'queries/sites';

const SettingsDiv = styled(DivBlock)`
  display: flex;
  height: 500px;
  width: 100%;
`;

const InputBox = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`;

const MainSettingsTab = ({ pageId, name, settings }) => (
  <Tab>
    <Formik
      initialValues={{ pageName: name }}
      onSubmit={(values, actions) => {
        onSubmit(actions, UpdatePageSettingsM, { id: pageId, name: values.pageName }, result => {
          new Noty({
            text: 'Изменения успешно сохранены'
          }).show();
        });
      }}
      validate={values => {
        const errors = {};

        if (!values.pageName) {
          errors.pageName = 'Обязательное поле';
        }

        return errors;
      }}
    >
      {({ errors, values, submitForm, isSubmitting }) => (
        <MainSettingsForm
          errors={errors}
          values={values}
          submitForm={submitForm}
          isSubmitting={isSubmitting}
        />
      )}
    </Formik>
  </Tab>
);

const MainSettingsForm = ({ errors, values, submitForm, isSubmitting }) => (
  <Form>
    <InputBox>
      <FormikInput
        label="Название страницы"
        name="pageName"
        isError={errors.pageName !== undefined}
        value={values.pageName}
      />
      <ErrorMessage component={ErrorBox} name="pageName" />
    </InputBox>
    <Button disabled={isSubmitting} type="submit" text="Сохранить" />
  </Form>
);

const DeletePageTab = ({ pageId, history, match }) => {
  const [deleteSitePage] = useMutation(DeleteSitePageM, {
    onCompleted: data => {
      history.push(`/sites/${match.params.siteId}`);
    }
  });
  const [isDeletionModalOpen, setIsDeletionModalOpen] = useState(false);

  return (
    <Tab>
      <QuestionModal
        isOpen={isDeletionModalOpen}
        onRequestClose={() => {
          setIsDeletionModalOpen(false);
        }}
        questionText="Вы точно хотите удалить страницу? У вас не будет возможности ее восстановить."
        okText="Удалить страницу"
        okAction={() => {
          deleteSitePage({ variables: { id: pageId } });
        }}
        cancelAction={() => {
          setIsDeletionModalOpen(false);
        }}
        height="75px"
      />
      <Button
        onClick={() => {
          setIsDeletionModalOpen(true);
        }}
        text="Удалить страницу"
      />
    </Tab>
  );
};

const ComponentsMap = {};

ComponentsMap[MainSettingsTab.name] = MainSettingsTab;
ComponentsMap[DeletePageTab.name] = DeletePageTab;

const PageSettings = ({ match, history }) => {
  const [selectedTab, setSelectedTab] = useState('MainSettingsTab');
  const [settings, setSettings] = useState(null);

  const { loading, data } = useQuery(PageSettingsQ, {
    variables: { id: match.params.id },
    fetchPolicy: 'network-only',
    onCompleted: data_ => {
      if (!data_.sitePage) {
        return;
      }

      if (data_.sitePage.jsonSettings) {
        setSettings(JSON.parse(data_.sitePage.jsonSettings));
      }
    }
  });

  const [updateSettingsM] = useMutation(UpdatePageSettingsM, {
    onCompleted: data => {}
  });

  const settingsCopy = cloneDeep(settings);

  return (
    <>
      <Content>
        <Header selected="sites" />
        <OuterFlex>
          <InnerFlex>
            <Title>Настройки страницы</Title>
            <SettingsDiv>
              <Menu
                setSelectedTab={setSelectedTab}
                links={{
                  MainSettingsTab: 'Общие настройки',
                  DeletePageTab: 'Удаление страницы'
                }}
              />
              {!loading && (
                <MenuView
                  componentsMap={ComponentsMap}
                  pageId={match.params.id}
                  component={selectedTab}
                  history={history}
                  settings={settingsCopy}
                  setSettings={setSettings}
                  updateSettingsM={updateSettingsM}
                  name={data.sitePage.name}
                  match={match}
                />
              )}
            </SettingsDiv>
          </InnerFlex>
        </OuterFlex>
      </Content>
      <Footer />
    </>
  );
};

export default PageSettings;
