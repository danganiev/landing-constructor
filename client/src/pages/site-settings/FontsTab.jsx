import React, { useState } from 'react';
import styled from 'styled-components';
import { useMutation, useQuery } from 'react-apollo';
import { map, last, assign, each } from 'lodash';

import Button, { IconButton } from 'components/Button';
import { ValueCbDropdown } from 'components/inputs/Dropdown';
import { Tab } from 'components/TabMenu';
import { InnerFlex } from 'components/LayoutComponents';
import UploadFontModal from 'components/modals/UploadFontModal';

import { SiteFontsQ, DeleteSiteFileM } from 'queries/site-settings';
import { DEFAULT_FONTS } from 'utils/constants';
// import apolloClient from 'utils/apolloClient';

const InputBox = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`;

const FontsTable = ({ setIsModalOpen, siteFonts, fontsRefetch }) => {
  const [deleteSiteFile] = useMutation(DeleteSiteFileM, {
    onCompleted: () => {
      fontsRefetch();
    }
  });
  return (
    <>
      <InnerFlex style={{ width: '600px' }}>
        <span>Шрифты</span>
        <Button
          icon="plus"
          text="Добавить"
          onClick={() => {
            setIsModalOpen(true);
          }}
          style={{ marginRight: '0px' }}
        />
      </InnerFlex>
      <table style={{ border: 'black' }}>
        <thead>
          <tr>
            <th>Название</th>
            <th>Файл</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {map(siteFonts, (value, key) => {
            return (
              <tr key={key}>
                <td>{value.name}</td>
                <td>{last(value.file.url.split('/'))}</td>
                <td>
                  <IconButton
                    icon="trash"
                    onClick={() => {
                      deleteSiteFile({ variables: { siteFileId: value.id } });
                    }}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

const FontsTab = ({ settings, setSettings, siteId, updateSettingsM, token }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const siteFonts = useQuery(SiteFontsQ, {
    variables: { siteId },
    skip: !siteId
  });
  if (!settings) return <p />;

  let usedFonts = assign({}, DEFAULT_FONTS);

  if (!siteFonts.loading) {
    const userFonts = {};
    each(siteFonts.data.siteFonts, value => {
      userFonts[value.name] = {
        name: value.name,
        isUserFont: true,
        url: value.file.url
      };
    });
    usedFonts = assign(usedFonts, userFonts);
  }

  return (
    <Tab>
      <div>
        <InputBox>
          {/* .landing-title font-family */}
          <ValueCbDropdown
            label="Шрифт заголовков"
            options={usedFonts}
            onChange={ev => {
              const value = usedFonts[ev.target.value];
              settings.headerFont = ev.target.value;
              if (!value.isUserFont) {
                settings.headerFontCss = `.landing-title{font-family: "${value.name}",Arial,sans-serif;}`;
              } else {
                let ext = value.url.split('.');
                ext = ext[ext.length - 1];
                settings.headerFontCss = `
                @font-face{
                  font-family: "${value.name}";
                  src: url("${value.url}") format("${ext}")
                }
                .landing-title{font-family: "${value.name}",Arial,sans-serif;}`;
              }
              setSettings(settings);
            }}
            selected={settings.headerFont}
            valueCb={(value, key) => {
              return key;
            }}
          />
        </InputBox>
        <InputBox>
          {/* body font-family */}
          <ValueCbDropdown
            label="Шрифт остального текста"
            options={usedFonts}
            onChange={ev => {
              const value = usedFonts[ev.target.value];
              settings.bodyFont = ev.target.value;
              if (!value.isUserFont) {
                settings.bodyFontCss = `body{font-family: "${value.name}",Arial,sans-serif;}`;
              } else {
                let ext = value.url.split('.');
                ext = ext[ext.length - 1];
                settings.bodyFontCss = `
                @font-face{
                  font-family: "${value.name}";
                  src: url("${value.url}") format("${ext}")
                }
                body{font-family: "${value.name}",Arial,sans-serif;}`;
              }
              setSettings(settings);
            }}
            valueCb={(value, key) => {
              return key;
            }}
            selected={settings.bodyFont}
          />
        </InputBox>
        <Button
          type="submit"
          text="Сохранить"
          onClick={() => {
            updateSettingsM({
              variables: {
                id: siteId,
                jsonSettings: JSON.stringify(settings)
              }
            });
          }}
        />

        {siteFonts.loading ? null : (
          <FontsTable
            setIsModalOpen={setIsModalOpen}
            siteFonts={siteFonts.data.siteFonts}
            fontsRefetch={siteFonts.refetch}
          />
        )}
      </div>
      <UploadFontModal
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        siteId={siteId}
        uploadCb={(url, ...params) => {
          siteFonts.refetch();
        }}
        uploadCbParams={[]}
        token={token}
      />
    </Tab>
  );
};

export default FontsTab;
