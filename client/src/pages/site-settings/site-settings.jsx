/* eslint-disable max-classes-per-file */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/prefer-stateless-function */
import React, { useState } from 'react';
import styled from 'styled-components';
import { Query, useMutation, useQuery } from 'react-apollo';
import { Formik, Form, ErrorMessage } from 'formik';
import Noty from 'noty';
import { cloneDeep } from 'lodash';

import QuestionModal from 'components/modals/QuestionModal';
import { Header, Footer } from 'components/navigation';
import { Content, InnerFlex, OuterFlex, DivBlock, Title } from 'components/LayoutComponents';
import Button from 'components/Button';
import ErrorBox from 'components/ErrorBox';
import FormikInput from 'components/inputs/FormikInput';
import { Menu, Tab, MenuView } from 'components/TabMenu';

import onSubmit from 'utils/formSubmitter';
import { UpdateSettingsM, SettingsQ } from 'queries/site-settings';
import { DeleteSiteM } from 'queries/sites';

import FontsTab from './FontsTab';

const SettingsDiv = styled(DivBlock)`
  display: flex;
  height: 500px;
  width: 100%;
`;

const InputBox = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`;

const MainSettingsTab = ({ siteId, name }) => (
  <Tab>
    <Formik
      initialValues={{ siteName: name }}
      onSubmit={(values, actions) => {
        onSubmit(actions, UpdateSettingsM, { id: siteId, name: values.siteName }, result => {
          new Noty({
            text: 'Изменения успешно сохранены'
          }).show();
        });
      }}
      validate={values => {
        const errors = {};

        if (!values.siteName) {
          errors.siteName = 'Обязательное поле';
        }

        return errors;
      }}
    >
      {({ errors, values, submitForm, isSubmitting }) => (
        <MainSettingsForm
          errors={errors}
          values={values}
          submitForm={submitForm}
          isSubmitting={isSubmitting}
        />
      )}
    </Formik>
  </Tab>
);

const MainSettingsForm = ({ errors, values, submitForm, isSubmitting }) => (
  <Form>
    <InputBox>
      <FormikInput
        label="Название сайта"
        name="siteName"
        isError={errors.siteName !== undefined}
        value={values.siteName}
      />
      <ErrorMessage component={ErrorBox} name="siteName" />
    </InputBox>
    <Button disabled={isSubmitting} type="submit" text="Сохранить" />
    {/* <InputBox /> */}
  </Form>
);

const DeleteSiteTab = ({ siteId, history }) => {
  // const [deleteSite, mutationData] = useMutation(DeleteSiteM, {
  const [deleteSite] = useMutation(DeleteSiteM, {
    onCompleted: data => {
      history.push(`/sites/`);
    }
  });
  const [isSiteDeletionModalOpen, setIsSiteDeletionModalOpen] = useState(false);

  return (
    <Tab>
      <QuestionModal
        isOpen={isSiteDeletionModalOpen}
        onRequestClose={() => {
          setIsSiteDeletionModalOpen(false);
        }}
        questionText="Вы точно хотите удалить сайт? У вас не будет возможности его восстановить."
        okText="Удалить сайт"
        okAction={() => {
          deleteSite({ variables: { id: siteId } });
        }}
        cancelAction={() => {
          setIsSiteDeletionModalOpen(false);
        }}
        height="75px"
      />
      <Button
        onClick={() => {
          setIsSiteDeletionModalOpen(true);
        }}
        text="Удалить сайт"
      />
    </Tab>
  );
};

const ComponentsMap = {};

ComponentsMap[MainSettingsTab.name] = MainSettingsTab;
ComponentsMap[DeleteSiteTab.name] = DeleteSiteTab;
ComponentsMap[FontsTab.name] = FontsTab;

const SiteSettings = ({ match, history }) => {
  const [selectedTab, setSelectedTab] = useState('MainSettingsTab');
  const [settings, setSettings] = useState(null);

  const { data, loading } = useQuery(SettingsQ, {
    variables: { id: match.params.id },
    fetchPolicy: 'network-only',
    onCompleted: data_ => {
      if (!data_.site) {
        return;
      }

      if (data_.site.jsonSettings) {
        // try {
        setSettings(JSON.parse(data_.site.jsonSettings));
        // } catch (e) {
        //   // data.settings.jsonSettings = {};
        // }
      }
    }
  });

  const [updateSettingsM] = useMutation(UpdateSettingsM, {
    onCompleted: data => {}
  });

  // if (process.env.NODE_ENV === 'development' && selectedTab === 'MainSettingsTab') {
  //   setSelectedTab('FontsTab');
  // }

  const settingsCopy = cloneDeep(settings);

  return (
    <>
      <Content>
        <Header selected="sites" />
        <OuterFlex>
          <InnerFlex>
            <Title>Настройки сайта</Title>
            <SettingsDiv>
              <Menu
                setSelectedTab={setSelectedTab}
                links={{
                  MainSettingsTab: 'Общие настройки',
                  DeleteSiteTab: 'Удаление сайта',
                  FontsTab: 'Шрифты'
                }}
              />
              {!loading && (
                <MenuView
                  componentsMap={ComponentsMap}
                  siteId={match.params.id}
                  component={selectedTab}
                  history={history}
                  settings={settingsCopy}
                  setSettings={setSettings}
                  updateSettingsM={updateSettingsM}
                  name={data.site.name}
                />
              )}
            </SettingsDiv>
          </InnerFlex>
        </OuterFlex>
      </Content>
      <Footer />
    </>
  );
};

export default SiteSettings;
