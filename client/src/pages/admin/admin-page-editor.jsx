/* eslint-disable react/prop-types */
// @flow
import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { each } from 'lodash';
import Noty from 'noty';

import CategoriesSidebar from 'components/sidebars/CategoriesSidebar';
import TraitsSidebar from 'components/TraitsSidebar';
import GrapesEditor from 'components/GrapesEditor';
import AddButton from 'components/AddButton';
import Input from 'components/inputs/Input';
import Button from 'components/Button';
import Modal from 'components/modals/Modal';
import AdminPageEditorToolbar from 'components/toolbars/AdminPageEditorToolbar';
import constants from 'utils/constants';
import apolloClient from 'utils/apolloClient';
import { toggleSidebar, updateSectionsAndTraits } from 'reducers/editor';

import { PageTemplateQ } from 'queries/sites';
import { TraitsQ } from 'queries/sections';
import { UpsertPageTemplateM } from 'queries/admin';

const MainContainer = styled.div`
  height: 100%;
`;

const AddButtonContainer = styled.div`
  position: fixed;
  bottom: 30px;
  left: 30px;
  z-index: 1000;
`;

class AdminPageEditor extends React.Component {
  // state = {
  //   pageId: null
  // };

  constructor(props) {
    super(props);
    this.state = {
      pageId: props.match.params.id,
      page: null,
      isModalOpen: false
    };

    this.setState = this.setState.bind(this);
    this.savePage = this.savePage.bind(this);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    const { pageId } = this.state;

    if (pageId) {
      apolloClient
        .query({
          query: PageTemplateQ,
          variables: {
            id: pageId
          }
        })
        .then(result => {
          if (result.data) {
            this.setState({ page: result.data.pageTemplate });
          }
        });
    } else {
      this.setState({ page: { name: '' } });
    }

    apolloClient
      .query({
        query: TraitsQ
      })
      .then(result => {
        if (result.data) {
          const { data } = result;
          const sectionsAndTraits = {};
          each(data.allSections, value => {
            sectionsAndTraits[value.sectionId] = JSON.parse(value.traitsJson);
          });
          dispatch(updateSectionsAndTraits(sectionsAndTraits));
        }
      });
  }

  // savePage(page, html) {
  //   apolloClient
  //     .mutate({
  //       mutation: UpsertPageTemplateM,
  //       variables: {
  //         id: page.id,
  //         name: page.name,
  //         html
  //       }
  //     })
  //     .then(result => {
  //       this.setState({ page: result.data.upsertPageTemplate, isModalOpen: false });
  //     });
  // }

  savePage(silent) {
    const { editor } = this.props;
    const { page } = this.state;

    // console.log(editor.getHtml());
    // console.log('----------');
    // console.log(editor.getCss());
    apolloClient
      .mutate({
        mutation: UpsertPageTemplateM,
        variables: {
          id: page.id,
          name: page.name,
          html: editor.getHtml(),
          css: editor.getCss()
        }
      })
      .then(result => {
        if (silent) {
          return;
        }
        this.setState({ isModalOpen: false });

        new Noty({
          text: 'Изменения сохранены',
          type: 'success'
        }).show();
      });
  }

  render() {
    const { categories, selectedCategory, dispatch, match, editor, isToggled } = this.props;
    const { page, isModalOpen } = this.state;
    const { setState } = this;

    if (page === null) {
      return null;
    }

    return (
      <>
        <Modal
          isOpen={isModalOpen}
          onRequestClose={() => {
            setState({ isModalOpen: false });
          }}
          style={{
            // top: '400px',
            // bottom: '400px',
            // left: '350px',
            // right: '350px',
            minHeight: '200px',
            minWidth: '200px'
          }}
        >
          <Input
            label="Название:"
            value={page.name}
            onChange={event => {
              page.name = event.target.value;
              setState({ page });
            }}
          />
          <br />
          <Button
            title="Сохранить"
            onClick={() => {
              const finalHtml = `${editor.getHtml()}<style>${editor.getCss()}</style>`;
              this.savePage(page, finalHtml);
            }}
          />
        </Modal>
        <MainContainer>
          <AdminPageEditorToolbar
            toggleModal={() => {
              setState({ isModalOpen: true });
            }}
          />
          <div style={{ height: '100%', zIndex: 100 }}>
            <CategoriesSidebar
              sidebarToggled={isToggled}
              categories={categories}
              selectedCategory={selectedCategory}
            />
            <TraitsSidebar site={{ id: match.params.siteId }} savePage={this.savePage} />
            <div style={{ paddingTop: constants.HEADER_HEIGHT, height: '100%' }}>
              <GrapesEditor
                editorHTML={`<style>${page.css}</style>${page.html}`}
                pageId={match.params.pageId}
                isAdminTemplate
                saveFn={this.savePage}
              />
            </div>
          </div>
          <AddButtonContainer>
            <AddButton
              onClick={() => {
                dispatch(toggleSidebar());
              }}
            />
          </AddButtonContainer>
        </MainContainer>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isToggled: state.editor.isToggled,
  editor: state.editor.editor
});

export default connect(mapStateToProps)(AdminPageEditor);
