// @flow
import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { useQuery } from 'react-apollo';
import { useHistory } from 'react-router-dom';

import { Header, Footer } from 'components/navigation';
import Button, { LinkedButton, IconButton } from 'components/Button';
import { DivBlock, Content, InnerFlex, OuterFlex } from 'components/LayoutComponents';

import { PageTemplatesQ } from 'queries/sites';

const SitesFlex = styled.div`
  margin-top: 10px;
  display: flex;
  width: 100%;
  justify-content: center;
`;

const SitesGrid = styled.div`
  width: 800px;
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 10px;
`;

const SiteDiv = styled(DivBlock)`
  height: 60px;
  margin-bottom: 5px;
`;

const SiteDivFlex = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SiteTitle = styled.span`
  text-align: center;
  flex-basis: 80%;
`;

const SiteButtons = styled.span`
  font-family: Helvetica, Memetica, sans-serif;
  font-size: 12px;
  text-align: center;
  flex-basis: 20%;
  display: flex;
`;

const TemplatesGridComponent = () => {
  const { loading, error, data } = useQuery(PageTemplatesQ, {
    fetchPolicy: 'network-only'
  });

  if (loading) return null;
  if (error) return <p>Произошла ошибка :(</p>;

  return (
    <SitesFlex>
      <SitesGrid>
        {data.pageTemplates.map(({ id, name }) => (
          <SiteDiv key={id}>
            <SiteDivFlex>
              <SiteTitle>{name}</SiteTitle>
              <SiteButtons>
                <LinkedButton text="Редактировать" link={`/admin/page-editor/${id}`} />
              </SiteButtons>
            </SiteDivFlex>
          </SiteDiv>
        ))}
      </SitesGrid>
    </SitesFlex>
  );
};

const AdminPageTemplates = () => {
  const history = useHistory();
  return (
    <div>
      <Content>
        <Header selected="sites" />
        <OuterFlex>
          <InnerFlex>
            <span style={{ fontSize: '20px' }}>Шаблоны страниц</span>
            <Button
              icon="plus"
              text="Добавить"
              onClick={() => {
                history.push('/admin/page-editor/');
              }}
              style={{ marginRight: '0px' }}
            />
          </InnerFlex>
        </OuterFlex>
        <TemplatesGridComponent />
      </Content>
      <Footer />
    </div>
  );
};

export default connect()(AdminPageTemplates);
