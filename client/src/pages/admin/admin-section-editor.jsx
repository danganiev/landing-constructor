/* eslint-disable react/sort-comp */
/* eslint-disable react/prop-types */
// @flow
import React, { useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import AdminSectionEditorToolbar from 'components/toolbars/AdminSectionEditorToolbar';
import GrapesEditor from 'components/GrapesEditorSections';
import Input from 'components/inputs/Input';
import Button from 'components/Button';
import Checkbox from 'components/inputs/Checkbox';
import Textarea from 'components/inputs/Textarea';
import Modal from 'components/modals/Modal';
import SanitizerModal from 'components/modals/SanitizerModal';
import constants from 'utils/constants';
import apolloClient from 'utils/apolloClient';
import { SectionQ } from 'queries/sections';
import { UpsertAdminSectionM } from 'queries/admin';

const Header = styled.h3`
  padding-bottom: 10px;
  margin-top: 0px;
  width: 100%;
  text-align: center;
`;

const SaveModal = ({
  isModalOpen,
  setIsModalOpen,
  section,
  updateSection,
  saveAdminSection,
  editor
}) => {
  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={() => {
        setIsModalOpen(false);
      }}
      style={{
        minHeight: '330px',
        minWidth: '750px'
      }}
    >
      <Input
        label="Название:"
        value={section.name}
        onChange={event => {
          section.name = event.target.value;
          updateSection(section);
        }}
        style={{ margin: '10px' }}
      />
      <br />
      <Input
        label="Section ID:"
        value={section.sectionId}
        onChange={event => {
          section.sectionId = event.target.value;
          updateSection(section);
        }}
        style={{ margin: '10px' }}
      />
      <br />
      <Textarea
        label="Свойства:"
        value={section.traits}
        onChange={event => {
          section.traits = event.target.value;
          updateSection(section);
        }}
        style={{ margin: '0px 10px' }}
      />
      <br />
      <Textarea
        label="Описание:"
        value={section.description}
        onChange={event => {
          section.description = event.target.value;
          updateSection(section);
        }}
        style={{ margin: '0px 10px' }}
      />
      <br />
      <Checkbox
        label="Секция активна?"
        checked={section.active}
        onChange={event => {
          section.active = event.target.checked;
          updateSection(section);
        }}
      />
      <br />
      <Button
        text="Сохранить"
        onClick={() => {
          const finalHtml = `${editor.getHtml()}<style>${editor.getCss()}</style>`;
          saveAdminSection(section, finalHtml);
          setIsModalOpen(false);
        }}
      />
    </Modal>
  );
};

const MainContainer = styled.div`
  height: 100%;
`;

class SectionEditor extends React.Component {
  constructor() {
    super();

    this.state = {
      section: null,
      isModalOpen: false,
      isSanitizerOpen: false
    };

    this.saveAdminSection = this.saveAdminSection.bind(this);
    this.updateSection = this.updateSection.bind(this);
    this.setIsModalOpen = this.setIsModalOpen.bind(this);
    this.setIsSanitizerOpen = this.setIsSanitizerOpen.bind(this);
  }

  componentDidMount() {
    const { match } = this.props;
    if (match.params.id === undefined) {
      this.updateSection(null);
      return;
    }

    apolloClient
      .query({
        query: SectionQ,
        variables: {
          id: match.params.id
        }
      })
      .then(result => {
        if (result.data) {
          result.data.section.traits = result.data.section.traitsJson;
          this.updateSection(result.data.section);
        }
      });
  }

  saveAdminSection(section, html) {
    apolloClient
      .mutate({
        mutation: UpsertAdminSectionM,
        variables: {
          id: section.id,
          name: section.name,
          traits: section.traits,
          sectionId: section.sectionId,
          description: section.description,
          active: section.active,
          html
        }
      })
      .then(result => {
        result.data.upsertAdminSection.traits = result.data.upsertAdminSection.traitsJson;
        this.updateSection(result.data.upsertAdminSection);
      });
  }

  setIsModalOpen(isModalOpen) {
    this.setState({ isModalOpen });
  }

  setIsSanitizerOpen(isSanitizerOpen) {
    this.setState({ isSanitizerOpen });
  }

  updateSection(section) {
    if (section === null) {
      section = {
        name: '',
        id: null,
        sectionId: '',
        html: '<div><h1>Say hello</h1><h2>Wave goodbye</h2>',
        traits: '{}',
        description: '',
        active: false
      };
    }
    this.setState({ section });
  }

  render() {
    const { editor } = this.props;
    const { section, isModalOpen, isSanitizerOpen } = this.state;
    const { updateSection } = this;

    return (
      <>
        {section !== null ? (
          <>
            <SaveModal
              isModalOpen={isModalOpen}
              setIsModalOpen={this.setIsModalOpen}
              section={section}
              updateSection={this.updateSection}
              editor={editor}
              saveAdminSection={this.saveAdminSection}
            />
            <SanitizerModal
              isSanitizerOpen={isSanitizerOpen}
              toggleSanitizer={this.setIsSanitizerOpen}
            />
            <MainContainer>
              <AdminSectionEditorToolbar
                toggleModal={() => {
                  this.setIsModalOpen(true);
                }}
                section={section}
                updateSection={this.updateSection}
                toggleSanitizer={this.setIsSanitizerOpen}
              />
              <div style={{ height: '100%', zIndex: 100 }}>
                <div style={{ paddingTop: constants.HEADER_HEIGHT, height: '100%' }}>
                  <GrapesEditor editorHTML={section.html} />
                </div>
              </div>
            </MainContainer>
          </>
        ) : null}
      </>
    );
  }
}

const mapStateToProps = state => ({
  editor: state.editor.editor,
  textEditor: state.textEditor.editor
});

export default connect(mapStateToProps)(SectionEditor);
