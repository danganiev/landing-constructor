// @flow
import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { useQuery } from 'react-apollo';
import { useHistory, Link } from 'react-router-dom';
import { each } from 'lodash';

import { Header } from 'components/navigation';
import { DivBlock, Content } from 'components/LayoutComponents';

import { UserPermissionsQ } from 'queries/users';

const SitesFlex = styled.div`
  margin-top: 10px;
  display: flex;
  width: 100%;
  justify-content: center;
`;

const SitesGrid = styled.div`
  margin-top: 70px;
  width: 800px;
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 10px;
`;

const SiteDiv = styled(DivBlock)`
  height: 60px;
  margin-bottom: 5px;
`;

const SiteDivFlex = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SiteTitle = styled.span`
  text-align: center;
  flex-basis: 80%;
`;

const SiteButtons = styled.span`
  font-family: Helvetica, Memetica, sans-serif;
  font-size: 12px;
  text-align: center;
  flex-basis: 20%;
  display: flex;
`;

const MainComponent = () => {
  return (
    <SitesFlex>
      <SitesGrid>
        <SiteDiv>
          <SiteDivFlex>
            <SiteTitle>
              <Link to="/admin/sections/">Секции</Link>
            </SiteTitle>
          </SiteDivFlex>
        </SiteDiv>
        <SiteDiv>
          <SiteDivFlex>
            <SiteTitle>
              <Link to="/admin/page-templates/">Шаблоны страниц</Link>
            </SiteTitle>
          </SiteDivFlex>
        </SiteDiv>
      </SitesGrid>
    </SitesFlex>
  );
};

const AdminMain = () => {
  const history = useHistory();

  const { loading, error, data } = useQuery(UserPermissionsQ, {});

  if (loading) return null;

  // no UI if not admin example
  if (!data.user || data.user.permissions.length === 0) {
    return null;
  }

  let found = false;
  each(data.user.permissions, value => {
    if (!found) {
      found = value.name === 'Admin';
    }
  });

  if (!found) {
    return null;
  }

  return (
    <div>
      <Content>
        <Header selected="sites" />
        <MainComponent />
      </Content>
      <footer />
    </div>
  );
};

export default connect()(AdminMain);
