// @flow
import React from 'react';
import styled from 'styled-components';
import { useQuery, useMutation } from 'react-apollo';

import { Header, Footer } from 'components/navigation';
import { Content, InnerFlex, OuterFlex, DivBlock } from 'components/LayoutComponents';
import Button from 'components/Button';
import ImageScroller from 'components/ImageScroller';

import { PageTemplatesQ, CreatePageFromTemplateM } from 'queries/sites';
import constants from 'utils/constants';

const TemplatesFlex = styled.div`
  margin-top: 10px;
  display: flex;
  width: 100%;
  justify-content: center;
`;

const TemplatesGrid = styled.div`
  width: 100%;
  margin-right: 100px;
  margin-left: 100px;
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: repeat(auto-fill, 400px);
  grid-column-gap: 10px;
  grid-row-gap: 30px;
  justify-content: space-evenly;
`;

const ImageDiv = styled.div`
  border-bottom: 1px solid ${constants.DARK_GREY};
`;

const TemplateTitle = styled.div`
  margin: 10px;
  font-weight: bold;
`;

const Description = styled.div`
  margin: 10px;
  font-size: 12px;
  letter-spacing: 0.042em;
  min-height: 50px;
`;

const ChooseTemplateButton = styled.div`
  font-family: Helvetica, Memetica, sans-serif;
  font-size: 12px;
  text-align: center;
`;

const TemplatesGridComponent = ({ history, match }) => {
  const { loading, data } = useQuery(PageTemplatesQ, {});
  const { siteId } = match.params;
  const [createPageFromTemplate, mutationData] = useMutation(CreatePageFromTemplateM, {
    onCompleted: data => {
      history.push(`/editor/${data.createPageFromTemplate.id}`);
    }
  });

  if (loading) return <p>Загружаем шаблоны...</p>;
  if (mutationData.loading) return <p>Создаем страницу...</p>;

  return (
    <TemplatesGrid>
      {data.pageTemplates.map(({ id, name, description }) => (
        <DivBlock key={id} style={{ border: 'none' }}>
          <ImageDiv>
            <ImageScroller />
          </ImageDiv>
          <TemplateTitle>{name}</TemplateTitle>
          <Description>{description}</Description>
          <ChooseTemplateButton>
            <Button
              text="Выбрать"
              onClick={() => {
                createPageFromTemplate({
                  variables: { siteId, templateId: id }
                });
              }}
            />
          </ChooseTemplateButton>
        </DivBlock>
      ))}
    </TemplatesGrid>
  );
};

const CreatePage = ({ history, match }) => {
  return (
    <div>
      <Content>
        <Header selected="sites" />
        <OuterFlex>
          <InnerFlex style={{ justifyContent: 'center', paddingBottom: '30px' }}>
            <div style={{ fontSize: '20px' }}>Выберите шаблон страницы</div>
          </InnerFlex>
        </OuterFlex>
        <TemplatesFlex>
          <TemplatesGridComponent history={history} match={match} />
        </TemplatesFlex>
      </Content>
      <div style={{ height: '30px' }} />
      <Footer />
    </div>
  );
};

export default CreatePage;
