// @flow
import React, { useState } from 'react';
import styled from 'styled-components';
// import { connect } from 'react-redux';
import { useQuery } from 'react-apollo';
import { useHistory } from 'react-router-dom';

import { Header, Footer } from 'components/navigation';
import Button, { LinkedButton, IconButton } from 'components/Button';
import { DivBlock, Content, InnerFlex, OuterFlex } from 'components/LayoutComponents';
import PublishPageModal from 'components/modals/PublishPageModal';

import { SiteWithPagesQ } from 'queries/sites';
import constants from 'utils/constants';

const Flex = styled.div`
  margin-top: 10px;
  display: flex;
  width: 100%;
  justify-content: center;
`;

const Grid = styled.div`
  width: 800px;
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 10px;
`;

const PageDiv = styled(DivBlock)`
  height: 60px;
  margin-bottom: 5px;
`;

const PageDivFlex = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Title = styled.span`
  text-align: center;
  flex-basis: 80%;
`;

const Buttons = styled.span`
  font-family: Helvetica, Memetica, sans-serif;
  font-size: 12px;
  text-align: center;
  flex-basis: 22%;
  display: flex;
`;

const NewPageCreateOuter = styled.div`
  height: 100px;
  width: 800px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px dashed grey;

  &:hover {
    background-color: ${constants.DARK_GREY};
    cursor: pointer;
  }
`;

const NewPageCreateInner = styled.div`
  text-align: center;
`;

const NewPageCreateDiv = ({ siteId }) => {
  const history = useHistory();
  return (
    <NewPageCreateOuter
      onClick={() => {
        history.push(`/sites/${siteId}/create-page`);
      }}
    >
      <NewPageCreateInner>
        У вас еще нет ни одной страницы. Нажмите сюда, чтобы создать первую.
      </NewPageCreateInner>
    </NewPageCreateOuter>
  );
};

const GridComponent = ({ siteId }) => {
  const { loading, error, data } = useQuery(SiteWithPagesQ, {
    fetchPolicy: 'network-only',
    variables: { id: siteId }
  });

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [publishPageId, setPublishPageId] = useState(null);

  if (loading) return null;
  if (error) return <p>Произошла ошибка :(</p>;

  const { pages } = data.site;

  return (
    <Flex>
      {pages.length ? (
        <Grid>
          {pages.map(({ id, name }) => (
            <PageDiv key={id}>
              <PageDivFlex>
                <Title>{name}</Title>
                <Buttons>
                  <IconButton
                    icon="cog"
                    link={`/sites/${siteId}/page-settings/${id}`}
                    title="Настройки страницы"
                  />
                  <Button
                    text="Опубликовать"
                    onClick={() => {
                      setPublishPageId(id);
                      setIsModalOpen(true);
                    }}
                  />
                  <LinkedButton text="Редактировать" link={`/editor/${id}`} />
                </Buttons>
              </PageDivFlex>
            </PageDiv>
          ))}
        </Grid>
      ) : (
        <NewPageCreateDiv siteId={siteId} />
      )}
      <PublishPageModal
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        pageId={publishPageId}
      />
    </Flex>
  );
};

const Pages = ({ match }) => {
  const history = useHistory();

  return (
    <div>
      <Content>
        <Header selected="sites" />
        <OuterFlex>
          <InnerFlex>
            <span style={{ fontSize: '20px' }}> Страницы сайта</span>
            <Button
              text="Опубликовать всё"
              onClick={() => {
                alert(2);
              }}
            />
            <Button
              icon="plus"
              text="Добавить"
              onClick={() => {
                history.push(`/sites/${match.params.siteId}/create-page`);
              }}
              style={{ marginRight: '0px' }}
            />
          </InnerFlex>
        </OuterFlex>
        <GridComponent siteId={match.params.siteId} />
      </Content>
      <Footer />
    </div>
  );
};

// export default connect()(Pages);
export default Pages;
