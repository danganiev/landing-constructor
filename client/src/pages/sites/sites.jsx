// @flow
import React from 'react';
import styled from 'styled-components';
import { useQuery } from 'react-apollo';
import { useHistory } from 'react-router-dom';

import { Header, Footer } from 'components/navigation';
import Button, { LinkedButton, IconButton } from 'components/Button';
import { DivBlock, Content, InnerFlex, OuterFlex } from 'components/LayoutComponents';

import { SitesQ } from 'queries/sites';
import constants from 'utils/constants';

const SitesFlex = styled.div`
  margin-top: 10px;
  display: flex;
  width: 100%;
  justify-content: center;
`;

const SitesGrid = styled.div`
  width: 800px;
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 10px;
`;

const SiteDiv = styled(DivBlock)`
  height: 60px;
  margin-bottom: 5px;
`;

const SiteDivFlex = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SiteTitle = styled.span`
  text-align: center;
  flex-basis: 80%;
`;

const SiteButtons = styled.span`
  font-family: Helvetica, Memetica, sans-serif;
  font-size: 12px;
  text-align: center;
  flex-basis: 22%;
  display: flex;
`;

const NewSiteCreateOuter = styled.div`
  height: 100px;
  width: 800px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px dashed grey;

  &:hover {
    background-color: ${constants.DARK_GREY};
    cursor: pointer;
  }
`;

const NewSiteCreateInner = styled.div`
  text-align: center;
`;

const NewSiteCreateDiv = () => {
  const history = useHistory();
  return (
    <NewSiteCreateOuter
      onClick={() => {
        history.push('/create-site');
      }}
    >
      <NewSiteCreateInner>
        У вас еще нет ни одного сайта. Нажмите сюда, чтобы создать первый.
      </NewSiteCreateInner>
    </NewSiteCreateOuter>
  );
};

const SitesGridComponent = () => {
  const { loading, error, data } = useQuery(SitesQ, {
    fetchPolicy: 'network-only'
  });

  if (loading) return null;
  if (error) return <p>Произошла ошибка :(</p>;

  return (
    <SitesFlex>
      {data.sites.length ? (
        <SitesGrid>
          {data.sites.map(({ id, name }) => (
            <SiteDiv key={id}>
              <SiteDivFlex>
                <SiteTitle>{name}</SiteTitle>
                <SiteButtons>
                  <IconButton icon="cog" link={`/site-settings/${id}`} title="Настройки" />
                  <LinkedButton text="Редактировать" link={`/sites/${id}`} />
                </SiteButtons>
              </SiteDivFlex>
            </SiteDiv>
          ))}
        </SitesGrid>
      ) : (
        <NewSiteCreateDiv />
      )}
    </SitesFlex>
  );
};

const Sites = () => {
  const history = useHistory();
  return (
    <div>
      <Content>
        <Header selected="sites" />
        <OuterFlex>
          <InnerFlex>
            <span style={{ fontSize: '20px' }}>Мои сайты</span>
            <Button
              icon="plus"
              text="Добавить"
              onClick={() => {
                history.push('/create-site');
              }}
              style={{ marginRight: '0px' }}
            />
          </InnerFlex>
        </OuterFlex>
        {/* <SitesFlex>
          <NewSiteCreateDiv history={history} />
        </SitesFlex> */}
        <SitesGridComponent />
      </Content>
      <Footer />
    </div>
  );
};

export default Sites;
