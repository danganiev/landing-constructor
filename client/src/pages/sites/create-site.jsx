// @flow
import React from 'react';
import styled from 'styled-components';
// import { connect } from 'react-redux';
import { useQuery, useMutation } from 'react-apollo';

import { Header, Footer } from 'components/navigation';
import { Content, InnerFlex, OuterFlex, DivBlock } from 'components/LayoutComponents';
import Button from 'components/Button';
import ImageScroller from 'components/ImageScroller';

import { PageTemplatesQ, CreateSiteFromPageTemplateM } from 'queries/sites';
import constants from 'utils/constants';

const TemplatesFlex = styled.div`
  margin-top: 10px;
  display: flex;
  width: 100%;
  justify-content: center;
`;

const TemplatesGrid = styled.div`
  width: 100%;
  margin-right: 100px;
  margin-left: 100px;
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: repeat(auto-fill, 400px);
  grid-column-gap: 10px;
  grid-row-gap: 30px;
  justify-content: space-evenly;
`;

const ImageDiv = styled.div`
  border-bottom: 1px solid ${constants.DARK_GREY};
`;

const TemplateTitle = styled.div`
  margin: 10px;
  font-weight: bold;
`;

const Description = styled.div`
  margin: 10px;
  font-size: 12px;
  letter-spacing: 0.042em;
  min-height: 50px;
`;

const ChooseTemplateButton = styled.div`
  font-family: Helvetica, Memetica, sans-serif;
  font-size: 12px;
  text-align: center;
`;

const TemplatesGridComponent = ({ history }) => {
  const { loading, data } = useQuery(PageTemplatesQ, {});
  const [createSiteFromPageTemplate, mutationData] = useMutation(CreateSiteFromPageTemplateM, {
    onCompleted: data => {
      history.push(`/editor/${data.createSiteFromPageTemplate.pages[0].id}`);
    }
  });

  if (loading) return <p>Загружаем шаблоны...</p>;
  if (mutationData.loading) return <p>Создаем страницу...</p>;

  return (
    <TemplatesGrid>
      {data.pageTemplates.map(({ id, name, description }) => (
        <DivBlock key={id} style={{ border: 'none' }}>
          <ImageDiv>
            <ImageScroller />
          </ImageDiv>
          <TemplateTitle>{name}</TemplateTitle>
          <Description>{description}</Description>
          <ChooseTemplateButton>
            <Button
              text="Выбрать"
              onClick={() => {
                createSiteFromPageTemplate({
                  variables: { name: 'Мой сайт', templateId: id }
                });
              }}
            />
          </ChooseTemplateButton>
        </DivBlock>
      ))}
    </TemplatesGrid>
  );
};

// IDEA: По кнопке выбрать можно показывать модалку с инпутом названия страницы (когда будет мультистраничность)
// Хотя это лишний шаг на пути к редактору - так что наверное нет
const CreateSite = ({ history }) => {
  return (
    <div>
      <Content>
        <Header selected="sites" />
        <OuterFlex>
          <InnerFlex style={{ justifyContent: 'center', paddingBottom: '30px' }}>
            <div style={{ fontSize: '20px' }}>Выберите шаблон первой страницы нового сайта</div>
          </InnerFlex>
        </OuterFlex>
        <TemplatesFlex>
          <TemplatesGridComponent history={history} />
        </TemplatesFlex>
      </Content>
      <div style={{ height: '30px' }} />
      <Footer />
    </div>
  );
};

export default CreateSite;
