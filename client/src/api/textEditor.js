export function makeTextBold(editor) {
  if (!editor) {
    return;
  }
  editor.format('bold', !editor.getFormat().bold);
}

export function makeTextItalic(editor) {
  if (!editor) {
    return;
  }
  editor.format('italic', !editor.getFormat().italic);
}

export function makeTextUnderlined(editor) {
  if (!editor) {
    return;
  }
  editor.format('underline', !editor.getFormat().underline);
}

export function makeTextColored(editor, color) {
  if (!editor) {
    return;
  }
  editor.format('color', color);
}
