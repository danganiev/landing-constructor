import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
// import { routerMiddleware, connectRouter } from 'connected-react-router';
// import { createBrowserHistory } from 'history';
import { logger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import mainReducer from './reducers';

// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const composeEnhancer = compose;

// export const history = createBrowserHistory();

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['auth']
};

const persistedReducer = persistReducer(persistConfig, mainReducer);

const store = createStore(persistedReducer, composeEnhancer(applyMiddleware(thunk, logger)));

export const persistor = persistStore(store);

export default store;
