// import BaloonEditor from '@ckeditor/ckeditor5-build-balloon/build/ckeditor';

import 'grapesjs/dist/css/grapes.min.css';
import grapesjs from 'grapesjs';

import store from './store';
import { setIsTextBeingEdited } from './reducers/textEditor';
import { componentSelected, toggleComponentSettings } from './reducers/editor';
import RTEPlugin from './grapesPlugins/RTEPlugin';
import openImport from './grapesPlugins/OpenImport';

function initEditorStuff(editor, config, saveFn) {
  editor.on('rte:enable', rte => {
    store.dispatch(setIsTextBeingEdited(true));
  });

  editor.on('rte:disable', rte => {
    store.dispatch(setIsTextBeingEdited(false));
  });

  editor.on('component:selected', component => {
    store.dispatch(componentSelected(component));
  });

  editor.on('component:dragEnd', model => {
    saveFn();
  });

  editor.Commands.add('landing:component-settings', editor => {
    store.dispatch(toggleComponentSettings(true));
  });

  editor.Commands.add('import-html', openImport(editor, config));
}

function getEditor(components, editorHTML, customToolbar, token, siteId, saveFn) {
  const config = {
    // const editor = grapesjs.init({
    showoffsets: 1,
    showDevices: 1,
    noticeOnUnload: 0,
    container: '#gjs',
    height: '100%',
    // fromElement: true,
    storageManager: { autoload: 0 },
    components: components ? JSON.parse(components) : editorHTML,
    plugins: [RTEPlugin],
    panels: {
      defaults: []
    },
    richTextEditor: {
      customToolbar: customToolbar.current
    },
    assetManager: {
      upload: `${process.env.REACT_APP_STATIC_URL}/upload`,
      modalTitle: 'Загрузка изображений',
      uploadText: 'Кликните или перетащите сюда файлы для загрузки',
      addBtnText: 'Добавить',
      inputPlaceholder: 'http://путь/до/изображения.jpg',
      params: {
        siteId
      },
      headers: {
        authorization: token ? `JWT ${token}` : ''
      }
    }
  };

  const editor = grapesjs.init(config);

  initEditorStuff(editor, config, saveFn);

  editor.BlockManager.add('my-first-block', {
    label: 'Simple block',
    content: '<div>This is a simple block</div>'
  });

  return editor;
}

function getEditorForSections(components, editorHTML, customToolbar, token, sectionId) {
  const config = {
    showoffsets: 1,
    showDevices: 1,
    noticeOnUnload: 0,
    container: '#gjs',
    height: '100%',
    storageManager: { autoload: 0 },
    components: components ? JSON.parse(components) : editorHTML,
    plugins: [RTEPlugin],
    panels: {
      defaults: []
    },
    richTextEditor: {
      customToolbar: customToolbar.current
    },
    assetManager: {
      upload: `${process.env.REACT_APP_STATIC_URL}/upload`,
      modalTitle: 'Загрузка изображений',
      uploadText: 'Кликните или перетащите сюда файлы для загрузки',
      addBtnText: 'Добавить',
      inputPlaceholder: 'http://путь/до/изображения.jpg',
      params: {
        sectionId
      },
      headers: {
        authorization: token ? `JWT ${token}` : ''
      }
    },
    baseCss: `
    * {
      box-sizing: border-box;
    }
    html, body, #wrapper {
      min-height: 100%;
    }
    body {
      margin: 0;
      height: 100%;
      background-color: #fff
    }
    p{
      margin: 0
    }

    b,
    strong {
      font-weight: bold;
    }

    #wrapper {
      overflow: auto;
      overflow-x: hidden;
    }

    span > div {
      display: inline-block;
    }

    * ::-webkit-scrollbar-track {
      background: rgba(0, 0, 0, 0.1)
    }

    * ::-webkit-scrollbar-thumb {
      background: rgba(255, 255, 255, 0.2)
    }

    * ::-webkit-scrollbar {
      width: 10px
    }

    .ql-container {
    }

    .ql-container.ql-disabled .ql-tooltip {
      visibility: hidden;
    }
    .ql-container.ql-disabled .ql-editor ul[data-checked] > li::before {
      pointer-events: none;
    }
    .ql-clipboard {
      left: -100000px;
      height: 1px;
      overflow-y: hidden;
      position: absolute;
      top: 50%;
    }
    .ql-clipboard p {
      margin: 0;
      padding: 0;
    }
    // .ql-editor {
      // white-space: pre-wrap;
      // word-wrap: break-word;
    // }

    .ql-editor > * {
      cursor: text;
    }

    .ql-cursor {
      display: none;
    }
    `
  };

  const editor = grapesjs.init(config);

  initEditorStuff(editor, config);

  return editor;
}

export { getEditor, getEditorForSections };
