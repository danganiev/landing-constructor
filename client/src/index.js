// @flow
import React from 'react';
import ReactDOM from 'react-dom';

import Root from './root';
// import store, { history } from './store';
import store from './store';

import 'normalize.css';
import 'animate.css';
import '../node_modules/noty/lib/noty.css';
import '../node_modules/noty/lib/themes/light.css';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/css/v4-shims.css';
import './styles/main.css';

ReactDOM.render(<Root store={store} />, document.querySelector('#root'));
