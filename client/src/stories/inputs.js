import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Input from 'components/inputs/Input';

storiesOf('Input', module).add('Just an input', () => (
  <div style={{ margin: '100px' }}>
    <Input />
  </div>
));
