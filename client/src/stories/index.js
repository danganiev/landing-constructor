import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Button from '../components/Button';

import 'purecss/build/pure-min.css';

storiesOf('Button', module).add('with text', () => (
  <div style={{ margin: '100px' }}>
    <Button onClick={action('clicked')}>Hello Button</Button>
  </div>
));
