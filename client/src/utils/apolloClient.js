import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { InMemoryCache } from 'apollo-cache-inmemory';
import _ from 'lodash';

import { logout } from 'reducers/auth';
import store from '../store';

const httpLink = createHttpLink({
  uri: 'http://localhost:4000/graphql'
});

const authLink = setContext((request, { headers }) => {
  // get the authentication token from local storage if it exists
  const { token } = store.getState().auth;

  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `JWT ${token}` : ''
    }
  };
});

const authErrorLink = onError(({ graphQLErrors, networkError }) => {
  if (networkError && networkError.statusCode === 401) {
    store.dispatch(logout(store.dispatch));
    return;
  }

  _.each(graphQLErrors, (value, index) => {
    if (value.extensions.code === 'UNAUTHENTICATED') {
      store.dispatch(logout(store.dispatch));
      return false;
    }
  });
});

const client = new ApolloClient({
  link: authErrorLink.concat(authLink.concat(httpLink)),
  cache: new InMemoryCache()
});

export default client;
