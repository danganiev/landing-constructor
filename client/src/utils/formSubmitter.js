import _ from 'lodash';
import apolloClient from 'utils/apolloClient';

export default function onSubmit(actions, mutation, variables, callback) {
  actions.setSubmitting(true);

  apolloClient
    .mutate({
      mutation,
      variables
    })
    .then(result => {
      actions.setSubmitting(false);
      callback(result);
    })
    .catch(e => {
      _.each(e.graphQLErrors, (value, index) => {
        // if (value.landingErrorType === 'ValidationError') {
        if (value.extensions.code === 'SERVER_VALIDATION_ERROR') {
          actions.setErrors(value.extensions.exception.errors);
        }
      });
      actions.setSubmitting(false);
    });
}
