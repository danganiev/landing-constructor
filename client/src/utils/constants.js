const HEADER_HEIGHT = '60px';

// colors
const BLUE = '#006fff';
const DARKER_BLUE = '#0044ff';
const WHITE_GREY = 'rgba(250,250,250,1)';
const GREY = 'rgba(244,244,244,1)';
const DARK_GREY = 'rgba(210,210,210,1)';
const BLACK_GREY = 'rgba(30, 30, 30, 1)';

export const DEFAULT_FONTS = {
  Arial: { name: 'Arial', isUserFont: false },
  'Times New Roman': { name: 'Times New Roman', isUserFont: false }
  // structure for user fonts
  // Oswald: {
  //   name: 'Oswald',
  //   isUserFont: true,
  //   url:
  //     'http://localhost:3333/uploads/ck2esx1j401kk0764pm3oqntm/oswald-v30-cyrillic_latin-regular.woff2'
  // }
};

export default {
  HEADER_HEIGHT,
  BLUE,
  GREY,
  DARK_GREY,
  DARKER_BLUE,
  BLACK_GREY,
  WHITE_GREY,
  DEFAULT_FONTS
};
