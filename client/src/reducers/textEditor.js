const SET_CURRENT_TEXT_EDITOR = 'SET_CURRENT_TEXT_EDITOR';
const SET_TOOLBAR = 'SET_TOOLBAR';
const SET_IS_TEXT_BEING_EDITED = 'SET_IS_TEXT_BEING_EDITED';

export default function reducer(
  state = {
    editor: null,
    toolbar: null,
    isTextBeingEdited: false
  },
  action = {}
) {
  switch (action.type) {
    case SET_CURRENT_TEXT_EDITOR:
      return {
        ...state,
        editor: action.editor
      };
    case SET_TOOLBAR:
      return {
        ...state,
        toolbar: action.toolbar
      };
    case SET_IS_TEXT_BEING_EDITED:
      return {
        ...state,
        isTextBeingEdited: action.isTextBeingEdited
      };
    default:
      return state;
  }
}

export function setCurrentTextEditor(editor) {
  // window.currentEditor = editor;
  return {
    type: SET_CURRENT_TEXT_EDITOR,
    editor
  };
}

export function setToolbar(toolbar) {
  return {
    type: SET_TOOLBAR,
    toolbar
  };
}

export function setIsTextBeingEdited(value) {
  return {
    type: SET_IS_TEXT_BEING_EDITED,
    isTextBeingEdited: value
  };
}
