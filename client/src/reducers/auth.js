const UPDATE_TOKEN = 'UPDATE_TOKEN';

export default function reducer(
  state = {
    token: null
  },
  action = {}
) {
  switch (action.type) {
    case UPDATE_TOKEN:
      return {
        ...state,
        token: action.token
      };
    default:
      return state;
  }
}

export function updateToken(response) {
  return {
    type: UPDATE_TOKEN,
    token: response.token
  };
}

export function logout(dispatch) {
  dispatch(updateToken({ token: null }));
  // history.push('/login/');
  // this will clear the history object, but w/e
  window.location = '/login';

  return {
    type: 'USER_LOGOUT'
  };
}
