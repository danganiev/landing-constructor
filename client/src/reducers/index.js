import { combineReducers } from 'redux';

import authReducer from './auth';
import editorReducer from './editor';
import textEditorReducer from './textEditor';

const appReducer = combineReducers({
  auth: authReducer,
  editor: editorReducer,
  textEditor: textEditorReducer
});

export default function rootReducer(state, action) {
  if (action.type === 'USER_LOGOUT') {
    state = undefined;
  }
  // else if (action.type === 'NO_CHANGES'){

  // }

  return appReducer(state, action);
}
