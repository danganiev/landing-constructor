const SET_EDITOR = 'SET_EDITOR';
const COMPONENT_SELECTED = 'COMPONENT_SELECTED';
const TOGGLE_COMPONENT_SETTINGS = 'TOGGLE_COMPONENT_SETTINGS';
const UPDATE_SECTIONS_AND_TRAITS = 'UPDATE_SECTIONS_AND_TRAITS';
const SIDEBAR_TOGGLE = 'SIDEBAR_TOGGLE';

export default function reducer(
  state = {
    editor: null,
    selectedComponent: null,
    componentSettingsToggled: false,
    sectionsAndTraits: {},
    isToggled: false
  },
  action = {}
) {
  switch (action.type) {
    case SET_EDITOR:
      return {
        ...state,
        editor: action.editor
      };
    case COMPONENT_SELECTED:
      return {
        ...state,
        selectedComponent: action.component
      };
    case TOGGLE_COMPONENT_SETTINGS:
      return { ...state, componentSettingsToggled: action.value };
    case UPDATE_SECTIONS_AND_TRAITS:
      return {
        ...state,
        sectionsAndTraits: Object.assign(state.sectionsAndTraits, action.st)
      };
    case SIDEBAR_TOGGLE:
      return {
        ...state,
        isToggled: !state.isToggled
      };
    default:
      return state;
  }
}

export function setEditor(editor) {
  return {
    type: SET_EDITOR,
    editor
  };
}

export function componentSelected(component) {
  return {
    type: COMPONENT_SELECTED,
    component
  };
}

export function toggleSidebar() {
  return {
    type: SIDEBAR_TOGGLE
  };
}

export function toggleComponentSettings(value) {
  return {
    type: TOGGLE_COMPONENT_SETTINGS,
    value
  };
}

export function updateSectionsAndTraits(st) {
  return {
    type: UPDATE_SECTIONS_AND_TRAITS,
    st
  };
}
