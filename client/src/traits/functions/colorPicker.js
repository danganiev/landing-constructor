export function colorPicker(gjsComponent, colorType, value) {
  const style = {};
  style[colorType] = value;
  gjsComponent.setStyle(style);
}
