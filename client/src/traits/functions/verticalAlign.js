const verticalAlignOptions = {
  top: 'Сверху',
  middle: 'Посередине',
  bottom: 'Снизу'
};

function verticalAlign(gjsComponent, value) {
  gjsComponent.setStyle({
    'vertical-align': value,
    display: 'table-cell',
    height: '100vh',
    'background-color': 'red'
  });
}

export { verticalAlign, verticalAlignOptions };

// Структура информации о трейтах в секции
// {name: {label: '', traitName:'', childComponentId: ''}}
// componentId это не то же самое что айди атрибут дом элемента, т.к пользователь может поменять обычный
// айдишник

// Test
// {'verticalAlign': {'label': 'align', 'traitname':'verticalAlign', 'childComponentId': 'testId'}
