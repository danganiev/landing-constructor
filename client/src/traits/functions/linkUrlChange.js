function linkUrlChange(gjsComponent, value) {
  gjsComponent.setAttributes({
    href: value
  });
}

export { linkUrlChange };
