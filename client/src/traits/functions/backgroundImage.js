/* eslint-disable import/prefer-default-export */
export function backgroundImage(url, traitsData, gjsComponent, editor) {
  editor.CssComposer.setRule(`.${traitsData.className}`, { 'background-image': `url(${url})` });

  // grapesjs использует самописный рофляный shallowDiff, поэтому нужно сначала доставать аттрибуты
  const attrs = gjsComponent.getAttributes();
  attrs['data-landing-default-backgroundimage'] = url;
  gjsComponent.setAttributes(attrs);
}
