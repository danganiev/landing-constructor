import React, { useState } from 'react';
import VerticalAlign from './controls/VerticalAlign';
import BackgroundImage from './controls/BackgroundImage';
import ColorPicker from './controls/ColorPicker';
import LinkUrlChange from './controls/LinkUrlChange';

const traitControls = {
  verticalAlign: VerticalAlign,
  backgroundImage: BackgroundImage,
  colorPicker: ColorPicker,
  linkUrlChange: LinkUrlChange
};

const TraitControl = ({
  controlName,
  children,
  selectedComponent,
  traitsData,
  // editor,
  // site,
  ...props
}) => {
  const gjsComponent = selectedComponent.findByAttr(
    'data-landing-componentid',
    traitsData.childComponentId
  )[0];

  const [defaultValue, setDefaultValue] = useState(
    gjsComponent
      ? gjsComponent.getAttributes()[`data-landing-default-${controlName.toLowerCase()}`]
      : null
  );

  if (!gjsComponent) {
    console.warn("gjsComponent wasn't found");
    return null;
  }

  // const defaultValue = gjsComponent.getAttributes()[
  //   `data-landing-default-${controlName.toLowerCase()}`
  // ];

  const Control = traitControls[controlName];
  if (Control) {
    return (
      <Control
        gjsComponent={gjsComponent}
        label={traitsData.label}
        defaultValue={defaultValue}
        setDefaultValue={setDefaultValue}
        traitsData={traitsData}
        {...props}
      >
        {children}
      </Control>
    );
  }
  return null;
};

export default TraitControl;

// Структура глобального хранилища трейтов
// {'traitname': 'controlName'}
