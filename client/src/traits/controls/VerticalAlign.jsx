import React from 'react';
import Dropdown from 'traits/inputs/Dropdown';
import { verticalAlignOptions, verticalAlign } from 'traits/functions/verticalAlign';

const VerticalAlign = props => (
  <Dropdown
    options={verticalAlignOptions}
    onChange={event => {
      verticalAlign(props.gjsComponent, event.target.value);
    }}
    {...props}
  />
);

export default VerticalAlign;
