import React, { useState } from 'react';
import styled from 'styled-components';
import { CustomPicker, ChromePicker } from 'react-color';

import Button from 'components/Button';
import { colorPicker } from 'traits/functions/colorPicker';

const ColorCircleOut = styled.div`
  display: inline-block;
  padding: 5px;
  background: #fff;
  border-radius: 1px;
  box-shadow: 0 0 0 1px rgb(182, 182, 182);
  display: inline-block;
  cursor: pointer;
`;
const ColorCircleIn = styled.div`
  border-radius: 2px;
  background: ${props => props.color};
  height: 14px;
  width: 36px;
  cursor: pointer;
`;

const Popover = styled.div`
  position: absolute;
  z-index: 1001;
`;

const Cover = styled.div`
  position: fixed;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
`;

const ColorPicker = ({
  gjsComponent,
  editor,
  label,
  defaultValue,
  setDefaultValue,
  traitsData
}) => {
  const [displayPicker, toggleDisplayPicker] = useState(false);
  const [color, setColor] = useState(defaultValue);

  return (
    <div style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
      <span style={{ marginLeft: '10px', marginRight: '10px' }}>{label}</span>
      <ColorCircleOut>
        <ColorCircleIn color={color} onClick={() => toggleDisplayPicker(!displayPicker)} />
      </ColorCircleOut>
      {displayPicker ? (
        <Popover>
          <Cover onClick={() => toggleDisplayPicker(false)} />
          <ChromePicker
            color={color}
            onChangeComplete={color => {
              setColor(color.hex);
              colorPicker(gjsComponent, traitsData.colorType || 'background-color', color.hex);
            }}
            disableAlpha
          />
        </Popover>
      ) : null}
    </div>
  );
};

export default CustomPicker(ColorPicker);
