import React, { useState } from 'react';
import Button from 'components/Button';
import { backgroundImage } from 'traits/functions/backgroundImage';
import UploadImageModal from 'components/modals/UploadImageModal';

const BackgroundImage = ({
  gjsComponent,
  editor,
  site,
  defaultValue,
  setDefaultValue,
  token,
  traitsData
  // ...props
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  return (
    <>
      <UploadImageModal
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        siteId={site.id}
        uploadCb={(url, ...params) => {
          backgroundImage(url, traitsData, ...params);
          setDefaultValue(url);
        }}
        uploadCbParams={[gjsComponent, editor]}
        token={token}
      />
      <div style={{ marginLeft: '10px' }}>
        <div>Фоновое изображение</div>
        <img width="200" src={defaultValue} alt="Фоновое изображение" />
        <Button
          style={{ padding: '3px 10px 1px 10px', marginTop: '0px', marginLeft: '0px' }}
          text="Изменить"
          onClick={event => {
            setIsModalOpen(true);
          }}
        />
      </div>
    </>
  );
};

export default BackgroundImage;
