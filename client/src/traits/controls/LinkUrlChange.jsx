import React from 'react';
import Text from 'traits/inputs/Text';
import { linkUrlChange } from 'traits/functions/linkUrlChange';

const LinkUrlChange = props => (
  <Text
    onChange={event => {
      linkUrlChange(props.gjsComponent, event.target.value);
    }}
    {...props}
  />
);

export default LinkUrlChange;
