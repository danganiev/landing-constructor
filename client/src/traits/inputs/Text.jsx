import React from 'react';

import { Input, Label } from 'traits/inputs/Base';

const TextInput = ({ label, ...rest }) => (
  <div>
    <Label>{label}</Label>
    <Input {...rest} />
  </div>
);

export default TextInput;
