import styled from 'styled-components';

export const Label = styled.label`
  margin-left: 10px;
  display: inline-block;
  width: 50px;
`;

export const Input = styled.input`
  margin: 5px 10px;
  height: 20px;
`;

export const Select = styled.select`
  margin: 5px 10px;
  height: 30px;
`;
