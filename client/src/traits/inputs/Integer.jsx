import React from 'react';
// import styled from 'styled-components';

import { Input, Label } from 'traits/inputs/Base';

const IntegerInput = ({ label, ...rest }) => (
  <div>
    <Label>{label}</Label>
    <Input {...rest} />
  </div>
);

export default IntegerInput;
