import React from 'react';
// import styled from 'styled-components';
import { map } from 'lodash';

import { Select, Label } from 'traits/inputs/Base';

const Dropdown = ({ label, onChange, options }) => (
  <span>
    <Label>{label}</Label>
    <Select onChange={onChange}>
      {map(options, (value, key) => (
        <option key={key} value={key}>
          {value}
        </option>
      ))}
    </Select>
  </span>
);

export default Dropdown;
