import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, useLocation } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import { PersistGate } from 'redux-persist/integration/react';
import Noty from 'noty';

import apolloClient from 'utils/apolloClient';
import PageEditor from 'pages/page-editor';

import Login from 'pages/auth/login';
import Logout from 'pages/auth/logout';
import Registration from 'pages/auth/registration';
import RestorePassword from 'pages/auth/restore-password';
import Sites from 'pages/sites/sites';
import Pages from 'pages/sites/pages';
import Profile from 'pages/profile';
import Tariff from 'pages/tariff';
import SiteSettings from 'pages/site-settings/site-settings';
import PageSettings from 'pages/site-settings/page-settings';
import AdminSectionEditor from 'pages/admin/admin-section-editor';
import AdminPageEditor from 'pages/admin/admin-page-editor';
import AdminMainPage from 'pages/admin/main';
import AdminSections from 'pages/admin/sections';
import AdminPageTemplates from 'pages/admin/page-templates';
import CreateSite from 'pages/sites/create-site';
import CreatePage from 'pages/sites/create-page';
// import onSubmit from 'utils/formSubmitter';
import { persistor } from './store';

Noty.overrideDefaults({
  layout: 'bottomRight',
  animation: {
    open: 'animated fadeIn fast', // Animate.css class names
    close: 'animated fadeOut faster' // Animate.css class names
  },
  timeout: 5000,
  theme: 'light'
});

type Props = {
  store: {}
};

const ScrollToTop = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
};

const Root = (props: Props) => {
  const { store } = props;

  return (
    <ApolloProvider client={apolloClient}>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Router>
            <ScrollToTop />
            <div style={{ height: '100%' }}>
              <Route exact path="/" component={Sites} />
              <Route exact path="/editor/:pageId" component={PageEditor} />
              {/* Should be pageId but for now it's siteId */}
              {/* <Route exact path="/editor/:siteId" component={PageEditor} /> */}
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              <Route exact path="/registration" component={Registration} />
              <Route exact path="/restore-password" component={RestorePassword} />
              <Route exact path="/sites" component={Sites} />
              <Route exact path="/sites/:siteId/create-page" component={CreatePage} />
              <Route exact path="/sites/:siteId" component={Pages} />
              <Route exact path="/create-site" component={CreateSite} />
              <Route exact path="/profile" component={Profile} />
              <Route exact path="/tariff" component={Tariff} />
              <Route exact path="/site-settings/:id" component={SiteSettings} />
              <Route exact path="/sites/:siteId/page-settings/:id" component={PageSettings} />
              <Route exact path="/admin/main" component={AdminMainPage} />
              <Route exact path="/admin/sections" component={AdminSections} />
              <Route exact path="/admin/section-editor/:id" component={AdminSectionEditor} />
              <Route exact path="/admin/section-editor" component={AdminSectionEditor} />
              <Route exact path="/admin/page-templates" component={AdminPageTemplates} />
              <Route exact path="/admin/page-editor/:id" component={AdminPageEditor} />
              <Route exact path="/admin/page-editor" component={AdminPageEditor} />
            </div>
          </Router>
        </PersistGate>
      </Provider>
    </ApolloProvider>
  );
};

export default Root;
