install:
	docker-compose -f docker-compose.builder.yml run --rm install

dev:
	docker-compose up

backup:
	docker exec -t landing-constructor-postgres bash -c 'pg_dump landing_constructor_prisma -U postgres -F t | gzip>/backups/backup.tar.gz'

restore_backup:
	docker exec -t -i landing-constructor-postgres bash -c 'gunzip -k /backups/backup.tar.gz && pg_restore -c -U postgres -d landing_constructor_prisma -v /backups/backup.tar && rm /backups/backup.tar'

default_data:
	docker exec -t landing-constructor-postgres bash -c 'pg_dump landing_constructor_prisma -U postgres -F t | gzip>/backups/default_data.tar.gz'

restore_default_data:
	docker exec -t -i landing-constructor-postgres bash -c 'gunzip -k /backups/default_data.tar.gz && pg_restore -c -U postgres -d landing_constructor_prisma -v /backups/default_data.tar && rm /backups/default_data.tar'

default_sections:
	docker exec -t -i landing-constructor-postgres bash -c "pg_dump landing_constructor_prisma -t 'default\$$default.\"Section\"' -U landing_constructor -F t | gzip>/backups/sections.tar.gz"

clean_sections:
	docker exec -t -i landing-constructor-postgres bash -c "psql -U landing_constructor -d landing_constructor_prisma -c 'DELETE FROM \"default\$$default\".\"Section\"'"

restore_sections:
	docker exec -t -i landing-constructor-postgres bash -c "gunzip -k /backups/sections.tar.gz && pg_restore -a -U postgres -d landing_constructor_prisma -t 'Section' -v /backups/sections.tar && rm /backups/sections.tar"
