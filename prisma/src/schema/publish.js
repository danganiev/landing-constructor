import { gql } from 'apollo-server-express';

import { authResolverDecorator, doesPageBelongToUser } from '../auth';
import GatsbyGenerator from '../generators/gatsby-generator';
import UnoptimizedGenerator from '../generators/unoptimized-generator';
import prefixGenerator from '../api/prefixGenerator';

export const typeDef = gql`
  extend type Mutation {
    publishPage(pageId: ID!): GeneratedSite!
  }

  type GeneratedSite {
    id: ID!
    site: Site!
    generatedPages: [GeneratedPage!]!
  }

  type GeneratedPage {
    id: ID!
    url: String!
    page: SitePage!
    generatedSite: GeneratedSite!
  }
`;

export const resolvers = authResolverDecorator({
  Mutation: {
    // Создает generatedSite если его нет, добавляет в него одну страницу
    // и запускает публикацию сайта
    async publishPage(root, args, context) {
      // let sitePage = null;
      const { pageId } = args;
      try {
        // sitePage = await doesPageBelongToUser(context, pageId);
        await doesPageBelongToUser(context, pageId);
      } catch (e) {
        throw e;
      }

      const sitePageQ = `
        query ($id: ID!){
          sitePage(where: {id: $id})
          {
            id
            site {
              id,
              mainPage{
                id
              }
              urlPrefix
            }
            html
            css
          }
        }
      `;

      const sitePage = await context.prisma.$graphql(sitePageQ, { id: pageId });

      const siteId = sitePage.sitePage.site.id;

      const generatedSites = await context.prisma.generatedSites({
        where: { site: { id: siteId } }
      });

      let generatedSite = null;

      if (generatedSites.length > 0) {
        generatedSite = generatedSites[0];
      } else {
        generatedSite = await context.prisma.createGeneratedSite({
          user: {
            connect: { id: context.auth.user.id }
          },
          site: {
            connect: { id: siteId }
          },
          generatedPages: {
            create: [
              {
                page: { connect: { id: pageId } }
              }
            ]
          }
        });
      }

      const generatedPages = await context.prisma.generatedPages({
        where: { page: { id: pageId } }
      });
      let generatedPage = null;
      if (generatedPages.length > 0) {
        generatedPage = generatedPages[0];
      } else {
        generatedPage = await context.prisma.createGeneratedPage({
          page: { connect: { id: pageId } },
          generatedSite: { connect: { id: generatedSite.id } }
        });
      }

      // now we need to get all pages to generate, not only the newest one
      const pagesQ = `
        query($id: ID!) {
          generatedPages(where: { generatedSite: { id: $id } }) {
            id
            page {
              id
              html
              css
              url
              defaultUrl
            }
          }
        }
      `;
      const pages = await context.prisma.$graphql(pagesQ, { id: generatedSite.id });

      const unoptimizedGenerator = new UnoptimizedGenerator(context, {
        generatedPages: pages.generatedPages,
        generatedSite,
        site: sitePage.sitePage.site
      });

      return unoptimizedGenerator.generate().then(value => {
        // const generator = new GatsbyGenerator(context, {
        //   generatedPages: pages.generatedPages,
        //   generatedSite,
        //   site: sitePage.sitePage.site
        // });
        // const generatorPromise = generator.generate();
        return value;
      });
    }
  }
});
