import { gql, ForbiddenError } from 'apollo-server-express';
import bcrypt from 'bcrypt';

import { authResolverDecorator } from '../auth';
import elements from '../default_data/elements';

export const typeDef = gql`
  type Result {
    success: Boolean
  }

  extend type Mutation {
    seedTestData: Result!
    upsertAdminSection(
      id: ID
      name: String!
      html: String!
      traitsJson: String
      sectionId: String
      active: Boolean!
      description: String
    ): Section!
    upsertPageTemplate(id: ID, name: String, html: String!, css: String!): SitePageTemplate!
  }
`;

async function isUserAnAdmin(context) {
  const res = await context.prisma
    .user({
      id: context.auth.user.id
    })
    .permissions({
      where: {
        name: 'Admin'
      }
    })
    .then(result => {
      if (result.length === 0) {
        // wrong
        // throw new ForbiddenError('Операция невозможна');
        // right
        return Promise.reject(new ForbiddenError('Операция невозможна'));
      }

      return result;
    });

  return res;
}

export const resolvers = {
  Mutation: {
    // Seed data should remain since it creates user pass through bcrypt
    async seedTestData(root, args, context) {
      const count = await context.prisma
        .permissionsConnection()
        .aggregate()
        .count();
      if (count > 0) {
        return {
          success: false
        };
      }

      await context.prisma.createPermission({
        name: 'Admin'
      });

      bcrypt.hash('database1', 4).then(async hash => {
        await context.prisma.createUser({
          email: 'admin@admin.com',
          password: hash
        });
      });

      elements.forEach(async element => {
        await context.prisma.createElement(element);
      });
      console.log('Successfully seeded.');

      return {
        success: true
      };
    }
  }
};

export const resolversWithAuth = authResolverDecorator({
  Mutation: {
    async upsertAdminSection(root, args, context) {
      try {
        await isUserAnAdmin(context);
      } catch (e) {
        throw e;
      }

      return context.prisma.upsertSection({
        where: {
          // Костыль от бага призмы
          id: args.id === null || args.id === undefined ? '-1' : args.id
        },
        create: {
          name: args.name,
          html: args.html,
          traitsJson: args.traitsJson,
          sectionId: args.sectionId,
          active: args.active,
          description: args.description
        },
        update: {
          name: args.name,
          html: args.html,
          traitsJson: args.traitsJson,
          sectionId: args.sectionId,
          active: args.active,
          description: args.description
        }
      });
    },

    async upsertPageTemplate(root, args, context) {
      try {
        await isUserAnAdmin(context);
      } catch (e) {
        throw e;
      }

      return context.prisma.upsertSitePageTemplate({
        where: {
          id: args.id === null || args.id === undefined ? '-1' : args.id
        },
        create: {
          name: args.name,
          html: args.html,
          css: args.css
        },
        update: {
          name: args.name,
          html: args.html,
          css: args.css
        }
      });
    }
  }
});
