import { gql, UserInputError } from 'apollo-server-express';
import bcrypt from 'bcrypt';
import nodemailer from 'nodemailer';

import { authFunctionDecorator } from '../auth';
import { ServerValidationError } from '../exceptions';

const SALT_ROUNDS = parseInt(process.env.SALT_ROUNDS);

export const typeDef = gql`
  type User {
    id: ID!
    email: String!
    name: String
    password: String!
    sites: [Site!]!
    permissions: [Permission!]!
  }

  type Permission {
    id: ID!
    name: String!
  }

  input UserUpdateType {
    email: String
    name: String
    password: String
  }

  extend type Query {
    # users: [User!]!
    user: User!
  }

  extend type Mutation {
    createUser(email: String!, password: String!, name: String): Token
    updateUser(data: UserUpdateType!): User
    updatePassword(oldPassword: String!, newPassword: String!): BooleanResult
    restorePassword(email: String!): BooleanResult
  }
`;

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'daniil.ganiev@gmail.com',
    // TODO: take pass from env
    pass: ''
  }
});

export const resolvers = {
  Query: {
    // users(root, args, context) {
    //   if (!context.auth.hasSignedIn) {
    //     throw new Error("");
    //   }
    //   // console.log(context.auth.user);
    //   return context.prisma.users(args);
    // },
    user: authFunctionDecorator((root, args, context) => {
      return context.auth.user;
    })
  },
  Mutation: {
    async createUser(root, args, context) {
      const { password } = args;

      const userCheck = await context.prisma.user({ email: args.email });

      if (userCheck) {
        throw new ServerValidationError({ email: 'Пользователь с таким e-mail уже существует' });
      }

      // return bcrypt
      const hash = await bcrypt.hash(password, SALT_ROUNDS);
      // .then(hash => {
      const user = await context.prisma.createUser({
        email: args.email,
        password: hash,
        name: args.name
      });

      const token = context.auth.createJWT(user);

      return {
        token
      };
      // })

      // .catch(err => {
      //   return err;
      // });
    },
    updateUser: authFunctionDecorator((root, args, context) => {
      if (args.data.password) {
        throw new ServerValidationError({ password: 'Данный метод не должен обновлять пароли' });
      }

      return context.prisma.updateUser({
        data: args.data,
        where: {
          id: context.auth.user.id
        }
      });
    }),
    updatePassword: authFunctionDecorator(async (root, args, context) => {
      const { user } = context.auth;
      const match = await bcrypt.compare(args.oldPassword, user.password);

      if (!match) {
        throw new ServerValidationError({ password: 'Неверный пароль' });
      }

      await context.prisma.updateUser({
        data: {
          password: bcrypt.hashSync(args.newPassword, SALT_ROUNDS)
        },
        where: {
          id: user.id
        }
      });

      return {
        result: true
      };
    }),
    async restorePassword(root, args, context) {
      const user = await context.prisma.user({ email: args.email });

      if (!user) {
        // Trying to confuse potential hackers (although they still can detect registrated emails in registration query)
        return {
          result: true
        };
      }

      const newPass = `${Math.random()
        .toString(36)
        .substr(2, 8)}${Math.random()
        .toString(36)
        .substr(2, 8)}${Math.random()
        .toString(36)
        .substr(2, 8)}`;

      const mailOptions = {
        from: 'daniil.ganiev@gmail.com',
        to: user.email,
        subject: 'Восстановление пароля на Терции',
        html: `<p>${newPass}</p>`
      };

      transporter.sendMail(mailOptions, function(err, info) {
        if (err) console.log(err);
        else console.log(info);
      });

      await context.prisma.updateUser({
        data: {
          password: bcrypt.hashSync(newPass, SALT_ROUNDS)
        },
        where: {
          id: user.id
        }
      });

      return {
        result: true
      };
    }
  },
  User: {
    sites(root, args, context) {
      return context.prisma
        .user({
          id: root.id
        })
        .sites();
    },

    permissions(user, args, context) {
      return context.prisma
        .user({
          id: user.id
        })
        .permissions();
    }
  }
};
