import { gql } from 'apollo-server-express';
import bcrypt from 'bcrypt';
import { ServerValidationError } from '../exceptions';

export const typeDef = gql`
  extend type Query {
    tokenAuth(username: String!, password: String!): Token!
  }

  type Token {
    token: String!
  }
`;

export const resolvers = {
  Query: {
    async tokenAuth(root, args, context) {
      const user = await context.prisma.user({ email: args.username });

      if (!user) {
        throw new ServerValidationError({
          username: '',
          password: 'Неверное имя пользователя или пароль'
        });
      }

      const match = await bcrypt.compare(args.password, user.password);

      if (!match) {
        throw new ServerValidationError({
          username: '',
          password: 'Неверное имя пользователя или пароль'
        });
      }

      const token = context.auth.createJWT(user);

      return {
        token
      };
    }
  }
};
