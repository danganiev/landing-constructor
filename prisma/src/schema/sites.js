import { gql, ForbiddenError } from 'apollo-server-express';

import { authResolverDecorator, doesSiteBelongToUser } from '../auth';
import prefixGenerator from '../api/prefixGenerator';

export const typeDef = gql`
  type Site {
    id: ID!
    user: User!
    name: String!
    pages: [SitePage!]!
    jsonSettings: String!
    urlPrefix: String
  }

  type SitePage {
    id: ID!
    name: String!
    site: Site!
    # HTML and CSS fields for recreation in static site generator
    html: String
    css: String
    jsonSettings: String
  }

  type SitePageTemplate {
    id: ID!
    name: String
    html: String
    css: String
    description: String
    previewUrl: String
  }

  type CreateSiteResult {
    siteId: ID!
    firstPageId: ID!
  }

  extend type Query {
    sites: [Site!]!
    sitePage(id: ID!): SitePage
    pageTemplate(id: ID!): SitePageTemplate
    pageTemplates: [SitePageTemplate!]!
    singleSitePageBySite(id: ID!): SitePage
    site(id: ID!): Site!
  }

  extend type Mutation {
    createSiteFromPageTemplate(name: String!, templateId: ID!): Site!
    createPageFromTemplate(siteId: ID!, templateId: ID!): SitePage!
    updateSite(id: ID!, name: String, jsonSettings: String): Site!
    updateSitePage(id: ID!, html: String, css: String, name: String, jsonSettings: String): SitePage
    deleteSite(id: ID!): BooleanResult!
    deleteSitePage(id: ID!): BooleanResult!
  }
`;
// createSite(name: String!): Site

export const resolvers = authResolverDecorator(
  {
    Query: {
      async site(root, args, context) {
        try {
          await doesSiteBelongToUser(context, args.id);
        } catch (e) {
          throw e;
        }

        return context.prisma.site({
          id: args.id
          // user: { connect: { id: context.auth.user.id } }
        });
      },
      sites(root, args, context) {
        return context.prisma.sites({
          where: { user: { id: context.auth.user.id }, isDeleted: false }
        });
      },
      pageTemplate(root, args, context) {
        return context.prisma.sitePageTemplate({
          id: args.id
        });
      },
      pageTemplates(root, args, context) {
        return context.prisma.sitePageTemplates();
      },
      async sitePage(root, args, context) {
        const result = await context.prisma.$graphql(
          `
        query ($id: ID!){
          sitePage(where: {id: $id})
          {
            id,
            site {
              id
              jsonSettings
            }
            html
            css
            name
            jsonSettings
          }
        }
      `,
          { id: args.id }
        );

        try {
          await doesSiteBelongToUser(context, result.sitePage.site.id);
        } catch (e) {
          throw e;
        }

        return result.sitePage;
      }
    },
    Mutation: {
      // Might be useful later for blank template
      // createSite(root, args, context) {
      //   return context.prisma.createSite({
      //     name: args.name,
      //     user: {
      //       connect: { id: context.auth.user.id }
      //     }
      //   });
      // },
      async createSiteFromPageTemplate(root, args, context) {
        const template = await context.prisma.sitePageTemplate({
          id: args.templateId
        });

        let isPrefixFound = false;
        let i = 0;
        let site = null;
        while (!isPrefixFound && i < 10) {
          try {
            site = await context.prisma.createSite({
              name: args.name,
              user: {
                connect: { id: context.auth.user.id }
              },
              pages: {
                create: [
                  {
                    html: template.html,
                    css: template.css,
                    defaultUrl: prefixGenerator()
                  }
                ]
              },
              urlPrefix: prefixGenerator()
            });
            isPrefixFound = true;
          } catch (e) {
            i++;
          }
        }

        return site;
      },
      async createPageFromTemplate(root, args, context) {
        try {
          await doesSiteBelongToUser(context, args.siteId);
        } catch (e) {
          throw e;
        }

        const template = await context.prisma.sitePageTemplate({
          id: args.templateId
        });

        let isURLFound = false;
        let url = '';
        while (!isURLFound) {
          url = prefixGenerator();
          isURLFound = !(await context.prisma.$exists.sitePage({
            site: { connect: { id: args.siteId } },
            defaultUrl: url
          }));
        }

        return context.prisma.createSitePage({
          name: template.name,
          site: {
            connect: { id: args.siteId }
          },
          html: template.html,
          css: template.css,
          defaultUrl: url
        });
      },
      async updateSitePage(root, args, context) {
        const query = `
          query ($id: ID!){
            sitePage(where: {id: $id})
            {
              site {
                id
              }
            }
          }
          `;

        const sitePage = await context.prisma.$graphql(query, { id: args.id });

        const siteId = sitePage.sitePage.site.id;

        try {
          await doesSiteBelongToUser(context, siteId);
        } catch (e) {
          throw e;
        }

        const data = {};

        return context.prisma.updateSitePage({
          where: {
            id: args.id
          },
          data: {
            html: args.html,
            css: args.css,
            jsonSettings: args.jsonSettings,
            name: args.name
          }
        });
      },
      async updateSite(root, args, context) {
        try {
          await doesSiteBelongToUser(context, args.id);
        } catch (e) {
          throw e;
        }

        return context.prisma.updateSite({
          where: {
            id: args.id
          },
          data: {
            name: args.name,
            jsonSettings: args.jsonSettings
          }
        });
      },
      async deleteSite(root, args, context) {
        try {
          await doesSiteBelongToUser(context, args.id);
        } catch (e) {
          throw e;
        }

        // const site = await context.prisma.updateSite({
        await context.prisma.updateSite({
          where: {
            id: args.id
          },
          data: {
            isDeleted: true
          }
        });

        return {
          result: true
        };
      },
      async deleteSitePage(root, args, context) {
        const query = `
          query ($id: ID!){
            sitePage(where: {id: $id})
            {
              site {
                id
              }
            }
          }
          `;

        const sitePage = await context.prisma.$graphql(query, { id: args.id });

        const siteId = sitePage.sitePage.site.id;

        try {
          await doesSiteBelongToUser(context, siteId);
        } catch (e) {
          throw e;
        }

        await context.prisma.updateSitePage({
          where: {
            id: args.id
          },
          data: {
            isDeleted: true
          }
        });

        return {
          result: true
        };
      }
    },
    Site: {
      pages(root, args, context) {
        return context.prisma.site({ id: root.id }).pages({ where: { isDeleted: false } });
      }
    }
  },
  ['Query', 'Mutation', 'Site']
);
