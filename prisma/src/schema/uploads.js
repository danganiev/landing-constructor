// import fs from 'fs';
import fs from 'fs-extra';
import path from 'path';
import axios from 'axios';
import shortid from 'shortid';
import { gql, ForbiddenError } from 'apollo-server-express';

import { authResolverDecorator, doesSiteBelongToUser } from '../auth';
import deleteFile from '../api/deleteFile';

export const typeDef = gql`
  type URL {
    url: String!
  }

  type File {
    url: String!
  }

  type SiteFile {
    id: ID!
    name: String
    file: File!
  }

  extend type Query {
    siteFonts(siteId: ID!): [SiteFile!]!
  }

  extend type Mutation {
    uploadFromURL(siteId: ID!, url: String!): URL
    deleteSiteFile(siteFileId: ID!): BooleanResult
  }
`;

export const resolvers = authResolverDecorator({
  Query: {
    async siteFonts(root, args, context) {
      const doesSiteBelongToUser = await context.prisma.$exists.site({
        id: args.siteId,
        user: { id: context.auth.user.id }
      });

      if (!doesSiteBelongToUser) {
        throw new ForbiddenError('Операция невозможна');
      }

      const result = await context.prisma.$graphql(
        `query ($id: ID!){
          siteFiles(where: {site: {id: $id}, type: FONT})
          {
            id,
            file{
              url
            },
            name
          }
        }`,
        { id: args.siteId }
      );
      return result.siteFiles;
    }
  },
  Mutation: {
    async uploadFromURL(root, args, context) {
      // The real check should be by magic numbers in the start of the file
      // if (fileExt !== '.png' && fileExt !== '.jpg' && fileExt !== '.gif' && fileExt !== '.bmp') {
      //   throw new ForbiddenError('Расширение файла не поддерживается');
      // }

      if (!args.url.startsWith('http://') && !args.url.startsWith('https://')) {
        throw new ForbiddenError('Ссылка должна начинаться с http:// или https://');
      }

      const doesSiteBelongToUser = await context.prisma.$exists.site({
        id: args.siteId,
        user: { id: context.auth.user.id }
      });

      if (!doesSiteBelongToUser) {
        throw new ForbiddenError('Операция невозможна');
      }

      const response = await axios({
        url: args.url,
        method: 'GET',
        responseType: 'stream'
      });

      let fileExt = path.extname(args.url);
      const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
      const disposition = response.headers['content-disposition'];
      const matches = filenameRegex.exec(disposition);

      if (fileExt === '' || !fileExt) {
        if (matches != null && matches[1]) {
          const requestFilename = matches[1].replace(/['"]/g, '');
          fileExt = path.extname(requestFilename);
        } else {
          const contentType = response.headers['content-type'];
          switch (contentType) {
            case 'image/jpeg':
              fileExt = '.jpg';
              break;
            case 'image/bmp':
              fileExt = '.bmp';
              break;
            case 'image/png':
              fileExt = '.png';
              break;
            default:
              break;
          }
        }
      }

      if (fileExt === '' || !fileExt) {
        throw new ForbiddenError('Расширение файла не поддерживается');
      }

      const filename = `${shortid.generate()}${fileExt}`;
      const uploadsDir = `/srv/uploads/${args.siteId}/`;

      await fs.mkdir(uploadsDir, { recursive: true });

      const finalPath = path.resolve(uploadsDir, filename);
      const writer = fs.createWriteStream(finalPath);
      const url = `/uploads/${args.siteId}/${filename}`;

      response.data.pipe(writer);

      return new Promise((resolve, reject) => {
        writer.on('finish', async () => {
          const stats = fs.statSync(finalPath);
          const fileSizeInBytes = stats.size;
          const file = await context.prisma.createFile({
            filename,
            url,
            path: finalPath,
            user: { connect: { id: context.auth.getUserId() } },
            sizeInBytes: fileSizeInBytes
          });

          const siteFile = await context.prisma.createSiteFile({
            name: '',
            type: 'IMAGE',
            site: { connect: { id: args.siteId } },
            file: { connect: { id: file.id } }
          });
          resolve({ url });
        });
        writer.on('error', () => {
          reject();
        });
      });
    },
    async deleteSiteFile(root, args, context) {
      // const site = await context.prisma
      //   .siteFile({
      //     id: args.siteFileId
      //   })
      //   .site();

      const siteFile = await context.prisma.$graphql(
        `query ($id: ID!){
          siteFile(where:{id: $id}){
            id,
            file{
              id
              path
            },
            site{
              id
            }
          }
        }`,
        { id: args.siteFileId }
      );

      console.log(siteFile);

      try {
        await doesSiteBelongToUser(context, siteFile.siteFile.site.id);
      } catch (e) {
        throw e;
      }

      // delete file on hd first
      // then sitefile from db (file deletes by cascade deletion)
      deleteFile(siteFile.siteFile.file, context.prisma, async prisma => {
        await prisma.deleteSiteFile({ id: args.siteFileId });
      });

      // const site = await context.prisma.updateSite({
      //   where: {
      //     id: args.id
      //   },
      //   data: {
      //     isDeleted: true
      //   }
      // });

      return {
        result: true
      };
    }
  }
});
