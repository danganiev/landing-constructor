import { merge } from 'lodash';

import { typeDef as Query } from './query';
import { typeDef as Common } from './commonTypeDefs';
import { typeDef as User, resolvers as userResolvers } from './users';
import { typeDef as Site, resolvers as siteResolvers } from './sites';
import { typeDef as Auth, resolvers as authResolvers } from './auth';
import { typeDef as Section, resolvers as sectionResolvers } from './sections';
import { typeDef as Uploads, resolvers as uploadsResolvers } from './uploads';
import {
  typeDef as Admin,
  resolvers as adminResolvers,
  resolversWithAuth as adminResolversWithAuth
} from './admin';

import { typeDef as Publish, resolvers as websiteGeneratorResolvers } from './publish';

export const resolvers = merge(
  userResolvers,
  siteResolvers,
  authResolvers,
  websiteGeneratorResolvers,
  sectionResolvers,
  adminResolvers,
  adminResolversWithAuth,
  uploadsResolvers
);
export const typeDefs = [Query, Common, User, Site, Auth, Publish, Section, Admin, Uploads];
