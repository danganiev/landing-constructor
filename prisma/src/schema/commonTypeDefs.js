import { gql } from 'apollo-server-express';

export const typeDef = gql`
  type BooleanResult {
    result: Boolean
  }
`;
