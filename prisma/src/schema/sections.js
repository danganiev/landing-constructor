import { gql } from 'apollo-server-express';

import { authResolverDecorator } from '../auth';

export const typeDef = gql`
  type Element {
    id: ID!
    name: String!
    value: String!
  }

  type Section {
    id: ID!
    sectionId: String
    name: String!
    html: String!
    css: String
    category: SectionCategory!
    previewUrl: String
    traitsJson: String
    description: String
    active: Boolean!
  }

  type SectionCategory {
    id: ID!
    name: String!
  }

  extend type Query {
    elements: [Element!]!
    allSections: [Section!]!
    sections(categoryId: ID!): [Section!]!
    section(id: ID!): Section
    categories: [SectionCategory!]!
  }
`;

export const resolvers = authResolverDecorator({
  Query: {
    elements(root, args, context) {
      return context.prisma.elements();
    },

    sections(root, args, context) {
      return context.prisma.sections({
        where: { category: { id: args.categoryId }, active: true }
      });
    },

    allSections(root, args, context) {
      return context.prisma.sections({ where: { active: true } });
    },

    categories(root, args, context) {
      return context.prisma.sectionCategories();
    },

    section(root, args, context) {
      return context.prisma.section({
        id: args.id
      });
    }
  }
});
