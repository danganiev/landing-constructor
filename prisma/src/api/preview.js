import { each } from 'lodash';
import sanitizeHtml from 'sanitize-html';

import { prisma } from '../prisma-client';
import Auth from '../auth';

const preview = async (req, res) => {
  if (!req.headers.cookie) {
    return res.status(400).send('No cookie');
  }
  const cookie = req.headers.cookie.replace(' ', '');
  const cookies = cookie.split(';');
  const cookiesDict = {};

  each(cookies, value => {
    const kv = value.split('=');
    cookiesDict[kv[0]] = kv[1];
  });

  const auth = new Auth({ req, token: cookiesDict.jwtToken });

  try {
    await auth.authenticate();
  } catch (e) {
    return res.status(401).send('Not authenticated');
  }

  const { pageId } = req.query;

  if (!pageId) {
    return res.status(400).send('pageId required');
  }

  const sitePage = await prisma.$graphql(
    `
        query ($id: ID!){
          sitePage(where: {id: $id})
          {
            id,
            site{
              id
            }
            html
            css
          }
        }
      `,
    { id: pageId }
  );

  if (!sitePage.sitePage) {
    return res.status(400).send('No such page');
  }

  const siteId = sitePage.sitePage.site.id;

  const doesSiteBelongToUser = await prisma.$exists.site({
    id: siteId,
    user: { id: auth.user.id }
  });

  if (!doesSiteBelongToUser) {
    return res.status(403).send('Forbidden');
  }

  const sanitizeSettings = {
    disallowedTagsMode: 'escape',
    allowedTags: [
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'blockquote',
      'p',
      'a',
      'ul',
      'ol',
      'nl',
      'li',
      'b',
      'i',
      'strong',
      'em',
      'strike',
      'code',
      'hr',
      'br',
      'div',
      'table',
      'thead',
      'caption',
      'tbody',
      'tr',
      'th',
      'td',
      'pre',
      'iframe',
      'span',
      'section',
      'style'
    ],
    allowedAttributes: false
  };

  res.send(
    `<html><head>${sanitizeHtml(
      `<style>${sitePage.sitePage.css}</style>`,
      sanitizeSettings
    )}</head><body>${sanitizeHtml(sitePage.sitePage.html, sanitizeSettings)}</body></html>`
  );
};

export default preview;
