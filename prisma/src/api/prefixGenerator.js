const prefixGenerator = () => {
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  function baseAlphabet(n) {
    const digits = 'abcdefghijklmnopqrstuvwxyz';

    return digits[n];
  }

  let prefix = '';
  for (let i = 0; i < 5; i++) {
    prefix += baseAlphabet(getRandomInt(0, 25));
  }
  return prefix;
};

export default prefixGenerator;
