import fs from 'fs-extra';

const deleteFileFromHD = (file, prisma, deleteCb) => {
  fs.unlink(file.path)
    .then(async () => {
      deleteCb(prisma);
    })
    .catch(async e => {
      // for some reason there is no such file, then we just delete stuff from db
      if (e.code === 'ENOENT') {
        deleteCb(prisma);
      }
    });
};

const deleteFile = deleteFileFromHD;

export default deleteFile;
