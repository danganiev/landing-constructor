/* eslint-disable no-underscore-dangle */
/* eslint-disable func-names */
/* eslint-disable consistent-return */
import fs from 'fs-extra';
import { extname } from 'path';
import { AuthenticationError, ForbiddenError } from 'apollo-server-express';
import { prisma } from '../prisma-client';

import Auth from '../auth';

const upload_ = (type, extensions) =>
  async function(req, res) {
    if (Object.keys(req.files).length == 0) {
      return res.status(400).send('No files were uploaded.');
    }

    if (req.header('Authorization') === undefined) {
      return res.status(401).send(new AuthenticationError('No auth header'));
    }

    const auth = new Auth({ req });

    try {
      await auth.authenticate();
    } catch (e) {
      return res.status(401).send(new AuthenticationError(e));
    }

    const { siteId, name } = req.body;

    if (!siteId) {
      return res.status(400).send(new Error('siteId required'));
    }

    const doesSiteBelongToUser = await prisma.$exists.site({
      id: siteId,
      user: { id: auth.user.id }
    });

    if (!doesSiteBelongToUser) {
      return res.status(403).send(new ForbiddenError('Операция невозможна'));
    }

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    const sampleFile = req.files['files[]'] || req.files.file;
    const filename = sampleFile.name;

    if (!extensions.includes(extname(filename))) {
      // return res.status(400).send(new Error('wrong file extension'));
      return res.status(400).send('wrong file extension');
    }

    const path = `/srv/uploads/${siteId}/${filename}`;
    const url = `http://localhost:3333/uploads/${siteId}/${filename}`;

    const folderPath = `/srv/uploads/${siteId}`;
    try {
      await fs.mkdir(folderPath, { recursive: true });
    } catch (err) {
      return res.status(500).send(err);
    }

    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv(path, async function(err) {
      if (err) return res.status(500).send(err);

      const stats = fs.statSync(path);
      const fileSizeInBytes = stats.size;
      const file = await prisma.createFile({
        filename,
        url,
        path,
        user: { connect: { id: auth.getUserId() } },
        sizeInBytes: fileSizeInBytes
      });

      const siteFile = await prisma.createSiteFile({
        name,
        type,
        site: { connect: { id: siteId } },
        file: { connect: { id: file.id } }
      });

      res.json({
        data: [url]
      });
    });
  };

export const upload = upload_('IMAGE', ['.jpeg', '.jpg', '.png']);
export const uploadFont = upload_('FONT', ['.woff', '.woff2']);
