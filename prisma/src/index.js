import fs from 'fs-extra';
import express from 'express';
import fileUpload from 'express-fileupload';
import cors from 'cors';
import cluster from 'cluster';
import os from 'os';
import { ApolloServer, makeExecutableSchema, AuthenticationError } from 'apollo-server-express';

import { prisma } from './prisma-client';
import { typeDefs, resolvers } from './schema';
import Auth from './auth';
import { upload, uploadFont } from './api/upload';
import preview from './api/preview';

if (cluster.isMaster) {
  const cpuCount = os.cpus().length;

  fs.ensureDirSync('/srv/sites');
  fs.ensureDirSync('/srv/uploads');

  for (let i = 0; i < cpuCount; i += 1) {
    cluster.fork();
  }

  cluster.on('online', function(worker) {
    console.log(`Worker ${worker.process.pid} is online.`);
  });

  cluster.on('exit', function(worker) {
    console.log('Worker %d died :(', worker.id);
    cluster.fork();
  });
} else {
  require('dotenv').config();

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers
  });

  const server = new ApolloServer({
    schema,
    context: async ({ req, res }) => {
      const auth = new Auth({ req });

      try {
        await auth.authenticate();
      } catch (e) {
        throw new AuthenticationError(e);
      }

      return {
        ...req,
        prisma,
        auth
      };
    },
    // cors: {
    //   origin: APP_URL,
    //   credentials: true,
    //   optionsSuccessStatus: 200,
    //   methods: ['POST']
    // },
    formatError: error => {
      console.log(error);
      return error;
    },
    //   playground: process.env.NODE_ENV === 'development',
    playground: true,
    debug: true
    // debug: process.env.NODE_ENV === "development"
  });

  const app = express();
  app.request.prisma = prisma;

  server.applyMiddleware({ app });

  app.use(
    fileUpload({
      limits: {
        fileSize: 20000000
      },
      abortOnLimit: true
    })
  );

  const corsOptions = {
    credentials: true,
    // origin: "http://localhost:3000"
    origin: process.env.ORIGIN
  };

  app.options('/upload', cors(corsOptions));
  app.post('/upload', cors(corsOptions), upload);
  app.options('/upload-font', cors(corsOptions));
  app.post('/upload-font', cors(corsOptions), uploadFont);
  app.get('/preview', preview);

  // server.listen().then(({ url, server }) => {
  app.listen({ port: 4000 }, () => {
    console.log(`Server is running on localhost:4000${server.graphqlPath}`);
  });
}
