import Mustache from 'mustache';
import fs from 'fs-extra';
import path from 'path';
import uuid4 from 'uuid/v4';
import util from 'util';
import child_process from 'child_process';
import pino from 'pino';

Mustache.tags = ['{*', '*}'];

let template;
const templatePath = path.join(__dirname, '..', '..', 'template-unoptimized', 'index.html');

fs.readFile(templatePath, 'utf8', (err, data) => {
  if (err) throw err;
  template = data;
  Mustache.parse(template);
});

// All of this stuff will run inside 'backend' docker container
export default class UnoptimizedGenerator {
  constructor(context, args) {
    this.tmpDirName = '';
    this.context = context;
    this.args = args;
  }

  generateUnoptimizedFolder() {
    this.tmpDirName = `/tmp/landing${this.args.site.urlPrefix}/unoptimized/`;

    return fs.copy(path.join(__dirname, '..', '..', 'template-unoptimized'), this.tmpDirName);
  }

  generateEachPage() {
    console.log(this.args.generatedPages);
    const createPageOnHDD = page => {
      let fileName = ``;
      let styleName = ``;

      if (!page.page.url || page.page.url === '') {
        fileName = `${page.page.defaultUrl}.html`;
        styleName = `${page.page.defaultUrl}.css`;
      }

      if (this.args.site.mainPage && this.args.site.mainPage.id === page.page.id) {
        fileName = 'index.html';
        styleName = 'index.css';
      }

      const renderedTemplate = Mustache.render(template, {
        content: page.page.html,
        styleName
      });

      // TODO: styles for each page
      const resultFilePath = path.join(this.tmpDirName, fileName);
      const styleFilePath = path.join(this.tmpDirName, styleName);

      return fs.writeFile(resultFilePath, renderedTemplate).then(res => {
        return fs.writeFile(styleFilePath, page.page.css);
      });
    };

    const reflect = p =>
      p.then(
        r => ({ result: r, status: 'fulfilled' }),
        e => ({ result: e, status: 'rejected' })
      );

    const pagePromises = this.args.generatedPages.map(page => reflect(createPageOnHDD(page)));

    return Promise.all(pagePromises);
  }

  async generate() {
    await this.generateUnoptimizedFolder();
    await this.generateEachPage();

    const srvDirName = `/srv/sites/landing${this.args.site.urlPrefix}/unoptimized/`;
    // NOTE: There is a problem that everything that's in the folder is copied, so both /tmp/ and /srv/ folders
    // will have some clutter from previous builds, but for now w/e
    await fs.copy(this.tmpDirName, srvDirName);

    const generatedSiteQ = `
        query ($id: ID!){
          generatedSite(where: {id: $id})
          {
            id
            site {
              id,
              urlPrefix
            }
          }
        }
      `;
    // I can build this from scratch from this.args, but this feels more stable and long-termy
    const generatedSite = await this.context.prisma.$graphql(generatedSiteQ, {
      id: this.args.generatedSite.id
    });

    return generatedSite.generatedSite;
    // TODO: скопировать готовый билд из /tmp/ папки в неочищаемую папку
  }
}
