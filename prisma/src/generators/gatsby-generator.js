import Mustache from 'mustache';
import fs from 'fs-extra';
import path from 'path';
import uuid4 from 'uuid/v4';
import util from 'util';
import child_process from 'child_process';
import pino from 'pino';

const logger = pino('/var/log/landing-errors.log');

const exec = util.promisify(child_process.exec);

Mustache.tags = ['{*', '*}'];

// var parsedTemplate;
let template;
const templatePath = path.join(__dirname, '..', '..', 'template', 'src', 'pages', 'index.js');

fs.readFile(templatePath, 'utf8', (err, data) => {
  if (err) throw err;
  template = data;
  // parsedTemplate = Mustache.parse(template);
  Mustache.parse(template);
});

// All of this stuff would be run inside 'backend' docker container
export default class GatsbyGenerator {
  constructor(context, args) {
    this.tmpGatsbyDirName = '';
    this.context = context;
    this.args = args;
    // Update everything for urlPrefix (can be defined by user)
    // this.uuid = '';
  }

  generateGatsbyFolder() {
    this.tmpGatsbyDirName = `/tmp/landing${this.args.site.urlPrefix}/gatsby/`;

    return fs
      .copy(path.join(__dirname, '..', '..', 'template'), this.tmpGatsbyDirName)
      .then(val => {
        // This is badly hardcoded, but w/e for now
        return fs.symlink('/node_modules/', `${this.tmpGatsbyDirName}/node_modules`, error => {
          // if symlink exists we don't recreate it
          if (error && error.code === 'EEXIST') {
            return;
          }
          logger.error(error);
          if (process.env.NODE_ENV === 'development') {
            console.log(error);
          }
        });
      });
  }

  generateEachPage() {
    const createPageOnHDD = page => {
      const renderedTemplate = Mustache.render(template, {
        // content: "<h1>Hello from nodejs</h1>"
        // content: this.args.content
        content: page.page.html
      });

      // const resultFilePath = path.join(this.tmpGatsbyDirName, 'src', 'pages', 'index.js');
      let fileName = `${page.id}.js`;
      if (this.args.site.mainPage && this.args.site.mainPage.id === page.page.id) {
        fileName = 'index.js';
      }
      const resultFilePath = path.join(this.tmpGatsbyDirName, 'src', 'pages', fileName);

      return fs.writeFile(resultFilePath, renderedTemplate);
    };

    const reflect = p =>
      p.then(
        r => ({ result: r, status: 'fulfilled' }),
        e => ({ result: e, status: 'rejected' })
      );

    const pagePromises = this.args.generatedPages.map(page => reflect(createPageOnHDD(page)));

    return Promise.all(pagePromises);
  }

  generateGatsbyWebsite() {
    return exec('./node_modules/.bin/gatsby build', { cwd: this.tmpGatsbyDirName }).then(
      ({ stdout, stderr }) => {
        if (stderr) {
          throw new Error(stderr);
        }

        return this.context.prisma.createGeneratedSite({
          uuid: this.uuid,
          site: { connect: { id: this.args.siteId } }
        });
      }
    );
  }

  generate() {
    return this.generateGatsbyFolder().then(result => {
      return this.generateEachPage().then(result => {
        return this.generateGatsbyWebsite().then(result => {
          // 'end' file is a sign that
          return fs.writeFile(path.join(this.tmpGatsbyDirName, 'end'));
          // TODO: скопировать готовый билд из /tmp/ папки в неочищаемую папку
        });
      });
    });
  }
}
