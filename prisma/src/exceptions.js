import { ApolloError } from "apollo-server-express";

export class ServerValidationError extends ApolloError {
  constructor(properties) {
    super("Произошла ошибка валидации", "SERVER_VALIDATION_ERROR", {
      errors: properties
    });

    Object.defineProperty(this, "name", { value: "ServerValidationError" });
  }
}
