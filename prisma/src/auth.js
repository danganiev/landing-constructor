import { AuthenticationError, ForbiddenError } from 'apollo-server-core';
import _ from 'lodash';

require('dotenv').config();

const jwt = require('jsonwebtoken');

const { JWT_SECRET } = process.env;

// const ONE_MINUTE = 1000 * 60;
// const ONE_DAY = ONE_MINUTE * 60 * 24;
// const ONE_MONTH = ONE_DAY * 30;

const verifyOptions = {
  audience: process.env.JWT_AUDIENCE,
  algorithms: ['HS256'],
  maxAge: '7 days'
  // maxAge: "5 seconds"
};

const createJwt = payload =>
  jwt.sign(payload, JWT_SECRET, {
    algorithm: 'HS256',
    audience: verifyOptions.audience,
    expiresIn: '7 days'
    // expiresIn: "5 seconds"
  });

const authenticate = (req, token) =>
  new Promise((resolve, reject) => {
    // let token = null;

    if (!token && req.headers.authorization) {
      token = req.headers.authorization.split(' ')[1];
    }

    // if (token === null) {
    if (!token) {
      resolve(null);
    }

    jwt.verify(token, JWT_SECRET, verifyOptions, (err, payload) => {
      if (err) {
        reject(err);
      }
      resolve(payload);
    });
  });

export default class Auth {
  constructor({ req, token }) {
    this.req = req;
    this.token = token;
    this.user = null;
    this.isReady = false;
    this.hasSignedIn = false;
  }

  async authenticate() {
    const { req, token } = this;

    const payload = await authenticate(req, token);

    if (payload) {
      this.user = await req.prisma.user({ id: payload.uid });
      this.payload = payload;
      this.hasSignedIn = true;
    }
  }

  createJWT(user) {
    const token = createJwt({ uid: user.id });

    return token;
  }

  getUserId() {
    return this.payload.uid;
  }
}

export const authResolverDecorator = (resolvers, keys) => {
  if (keys === undefined) {
    keys = ['Query', 'Mutation'];
  }
  const newResolvers = {};
  _.each(keys, value => {
    newResolvers[value] = {};

    _.each(resolvers[value], (resValue, key) => {
      newResolvers[value][key] = authFunctionDecorator(resValue);
    });
  });

  return newResolvers;
};

export const authFunctionDecorator = fn => {
  return (root, args, context) => {
    if (context.auth.user === null || !context.auth.hasSignedIn) {
      throw new AuthenticationError('Пользователь не авторизован');
    }
    return fn(root, args, context);
  };
};

export async function doesSiteBelongToUser(context, siteId) {
  // По идее можно выделить это в фабрику по производству ошибок из промисов если будет нужно.
  return context.prisma.$exists
    .site({
      id: siteId,
      user: { id: context.auth.user.id }
    })
    .then(result => {
      if (!result) {
        // this works here but not in some other places, so I changed it to Promise.reject
        // Promises should never throw!
        // throw new ForbiddenError('Операция невозможна');
        return Promise.reject(new ForbiddenError('Операция невозможна'));
      }

      return result;
    });
}

export async function doesPageBelongToUser(context, pageId) {
  const query = `
    query ($id: ID!){
      sitePage(where: {id: $id})
      {
        id
        site {
          id
        }
        html
        css
      }
    }
    `;

  const sitePage = await context.prisma.$graphql(query, { id: pageId });
  const siteId = sitePage.sitePage.site.id;

  try {
    await doesSiteBelongToUser(context, siteId);
  } catch (e) {
    throw e;
  }
}
